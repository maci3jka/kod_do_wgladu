# -*- coding: utf-8 -*-
from diag_tests.models import Diag, DiagGroup
from openpyxl import load_workbook

wb = load_workbook(filename = 'CENNIK-KOMERCYJNY-NOWY-SĄCZ-1.xlsx', read_only=True)

ws = wb["Nowy Sącz"]
for sheet in wb:
    print(sheet.title)
diag_group = None
for row in ws.rows:
    if row[0].value is None:
        continue

    if row[5].value is None:
        diag_group=row[0].value
        print(str(diag_group),end=" |")
        diag_group = DiagGroup.objects.create(title=diag_group)
    else:
        try:
            Diag.objects.create(title=row[0].value, icd=row[1].value, symbol=row[2].value, material=row[3].value, wait_time=row[4].value, price=row[5].value, adnotation=row[6].value, diag_group=diag_group)
        except ValueError:
            print("Print cannot put do db %s" %(str([ r.value for r  in row])))

        print(str(row[0].value),end=" |")
    print("\n------------------------------------")
