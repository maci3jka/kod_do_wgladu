"""wielomed URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from custom_user import views as custom_userViews
from news import views as newsViews
from doctors import views as doctorViews
from diag_tests import views as diag_testViews
from links import views as linksViews

urlpatterns = [
    url(r'^$', newsViews.home, name='home'),
    url(r'^news/(?P<news_id>[0-9]+)/$', newsViews.newsPage, name='news'),
    url(r'^contact/', custom_userViews.contact, name='contact'),
    url(r'^news_add/', newsViews.newsEdit, name='news_add'),
    url(r'^schedule/', doctorViews.doctors_dates, name='doctors_dates'),
    # url(r'^diagnostics/', diag_testViews.diag_list, name='diagnostics'),
    url(r'^diagnostics_detail/(?P<diag_group_id>[0-9]+)/', diag_testViews.diag_detail, name='diagnostics_detail'),
    url(r'^diagnostics_list/', diag_testViews.diagnostics_list, name='diagnostics_list'),
    url(r'^links/', linksViews.links, name='links'),
    url(r'^news_list/', newsViews.newsList, name='news_list'),
    url(r'^news_edit/(?P<news_id>[0-9]+)/$', newsViews.newsEdit, name='news_edit'),
    url(r'^pref/', custom_userViews.userEdit, name='pref'),
    url(r'^sick/', custom_userViews.sickEdit, name='sick'),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
