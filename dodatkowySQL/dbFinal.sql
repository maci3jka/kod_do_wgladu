-- czysczenie wszystkiego w schemie używać ostrożne
/*
drop schema public cascade;
create schema public;

*/
----------------tworzenie tablic, kluczy itp

CREATE TABLE stan_zgloszenia (
  idstan_zgloszenia SERIAL  NOT NULL ,
  stan VARCHAR(30)  UNIQUE    ,
PRIMARY KEY(idstan_zgloszenia));




CREATE TABLE Pracownik (
  idPracownik SERIAL  NOT NULL ,
  imie VARCHAR(30)   NOT NULL ,
  nazwisko VARCHAR(30)   NOT NULL ,
  email VARCHAR(30)   NOT NULL ,
  telefon VARCHAR(11)    ,
  posada VARCHAR(30)   NOT NULL ,
  login VARCHAR(10)   NOT NULL UNIQUE,
  haslo VARCHAR(8)   NOT NULL   ,
PRIMARY KEY(idPracownik));




CREATE TABLE Dziedzina (
  idDziedzina SERIAL  NOT NULL ,
  opisDziedzina VARCHAR(255)      ,
PRIMARY KEY(idDziedzina));




CREATE TABLE Uprawnienia (
  idUprawnienia  SERIAL  NOT NULL ,
  nazwa VARCHAR(15)  NOT NULL UNIQUE ,
  Opis VARCHAR(100)      ,
PRIMARY KEY(idUprawnienia));




CREATE TABLE Specjalista (
  idSpecjalista SERIAL  NOT NULL ,
  Pracownik_idPracownik INTEGER   NOT NULL UNIQUE   ,
  Szef_idPracownik INTEGER   NOT NULL   ,
PRIMARY KEY(idSpecjalista)    ,
  FOREIGN KEY(Pracownik_idPracownik)
    REFERENCES Pracownik(idPracownik),
  FOREIGN KEY(Szef_idPracownik)
    REFERENCES Pracownik(idPracownik));


CREATE INDEX Specjalista_FKIndex1 ON Specjalista (Pracownik_idPracownik);
CREATE INDEX Specjalista_FKIndex2 ON Specjalista (Szef_idPracownik);


CREATE INDEX IFK_Specjalista ON Specjalista (Pracownik_idPracownik);
CREATE INDEX IFK_SzefSpecjalisty ON Specjalista (Szef_idPracownik);


CREATE TABLE Dziedzina_has_Specjalista (
  Dziedzina_idDziedzina INTEGER   NOT NULL ,
  Specjalista_idSpecjalista INTEGER   NOT NULL   ,

PRIMARY KEY(Dziedzina_idDziedzina, Specjalista_idSpecjalista)    ,
  FOREIGN KEY(Dziedzina_idDziedzina)
    REFERENCES Dziedzina(idDziedzina),
  FOREIGN KEY(Specjalista_idSpecjalista)
    REFERENCES Specjalista(idSpecjalista));


CREATE INDEX Dziedzina_has_Specjalista_FKIndex1 ON Dziedzina_has_Specjalista (Dziedzina_idDziedzina);
CREATE INDEX Dziedzina_has_Specjalista_FKIndex2 ON Dziedzina_has_Specjalista (Specjalista_idSpecjalista);


CREATE INDEX IFK_Dziedzina_Specjalista_has ON Dziedzina_has_Specjalista (Dziedzina_idDziedzina);
CREATE INDEX IFK_Specjalista_Dziedzina_has ON Dziedzina_has_Specjalista (Specjalista_idSpecjalista);


CREATE TABLE Pracownik_has_Uprawnienia (
  Uprawnienia_idUprawnienia INTEGER   NOT NULL ,
  Pracownik_idPracownik INTEGER   NOT NULL   ,
PRIMARY KEY(Uprawnienia_idUprawnienia, Pracownik_idPracownik)    ,
  FOREIGN KEY(Uprawnienia_idUprawnienia)
    REFERENCES Uprawnienia(idUprawnienia),
  FOREIGN KEY(Pracownik_idPracownik)
    REFERENCES Pracownik(idPracownik));


CREATE INDEX Pracownik_has_Uprawnienia_FKIndex1 ON Pracownik_has_Uprawnienia (Uprawnienia_idUprawnienia);
CREATE INDEX Pracownik_has_Uprawnienia_FKIndex2 ON Pracownik_has_Uprawnienia (Pracownik_idPracownik);


CREATE INDEX IFK_Uprawnienia_Pracownik_has ON Pracownik_has_Uprawnienia (Uprawnienia_idUprawnienia);
CREATE INDEX IFK_Pracownik_Pracownik_has ON Pracownik_has_Uprawnienia (Pracownik_idPracownik);


CREATE TABLE Zgloszenie (
  idZgloszenie SERIAL   NOT NULL ,
  Pracownik_idPracownik INTEGER ,
  Szef_idPracownik INTEGER ,
  stan_zgloszenia_idstan_zgloszenia INTEGER   NOT NULL ,
  opis VARCHAR(255)   NOT NULL ,
  data_zgloszenia DATE   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  data_zamkniecia DATE DEFAULT CURRENT_TIMESTAMP CHECK(data_zgloszenia <= data_zamkniecia),
PRIMARY KEY(idZgloszenie)      ,
  FOREIGN KEY(stan_zgloszenia_idstan_zgloszenia)
    REFERENCES stan_zgloszenia(idstan_zgloszenia),
  FOREIGN KEY(Pracownik_idPracownik)
    REFERENCES Pracownik(idPracownik),
  FOREIGN KEY(Szef_idPracownik)
    REFERENCES Pracownik(idPracownik));


CREATE INDEX Zgloszenie_FKIndex1 ON Zgloszenie (stan_zgloszenia_idstan_zgloszenia);
CREATE INDEX Zgloszenie_FKIndex2 ON Zgloszenie (Pracownik_idPracownik);
CREATE INDEX Zgloszenie_FKIndex3 ON Zgloszenie (Szef_idPracownik);


CREATE INDEX IFK_Zgloszenie_data_zgloszenia ON Zgloszenie (stan_zgloszenia_idstan_zgloszenia);
CREATE INDEX IFK_Pracownik_zglaszajacy ON Zgloszenie (Pracownik_idPracownik);
CREATE INDEX IFK_Szef_zgloszenia ON Zgloszenie (Szef_idPracownik);


CREATE TABLE Sprawdzenie (
  Zgloszenie_idZgloszenie INTEGER   NOT NULL ,
  Specjalista_idSpecjalista INTEGER   NOT NULL   ,
PRIMARY KEY(Zgloszenie_idZgloszenie, Specjalista_idSpecjalista)    ,
  FOREIGN KEY(Zgloszenie_idZgloszenie)
    REFERENCES Zgloszenie(idZgloszenie),
  FOREIGN KEY(Specjalista_idSpecjalista)
    REFERENCES Specjalista(idSpecjalista));


CREATE INDEX Zgloszenie_has_Specjalista_FKIndex1 ON Sprawdzenie (Zgloszenie_idZgloszenie);
CREATE INDEX Zgloszenie_has_Specjalista_FKIndex2 ON Sprawdzenie (Specjalista_idSpecjalista);


CREATE INDEX IFK_Zgloszenie_Sprawdzenie ON Sprawdzenie (Zgloszenie_idZgloszenie);
CREATE INDEX IFK_Specjalista_Sprawdzenie ON Sprawdzenie (Specjalista_idSpecjalista);


CREATE TABLE Dziedzina_has_Zgloszenie (
  Dziedzina_idDziedzina INTEGER   NOT NULL ,
  Zgloszenie_idZgloszenie INTEGER   NOT NULL   ,
PRIMARY KEY(Dziedzina_idDziedzina, Zgloszenie_idZgloszenie)    ,
  FOREIGN KEY(Dziedzina_idDziedzina)
    REFERENCES Dziedzina(idDziedzina),
  FOREIGN KEY(Zgloszenie_idZgloszenie)
    REFERENCES Zgloszenie(idZgloszenie));


CREATE INDEX Dziedzina_has_Zgloszenie_FKIndex1 ON Dziedzina_has_Zgloszenie (Dziedzina_idDziedzina);
CREATE INDEX Dziedzina_has_Zgloszenie_FKIndex2 ON Dziedzina_has_Zgloszenie (Zgloszenie_idZgloszenie);


CREATE INDEX IFK_Dziedzina_Zgloszenie_has ON Dziedzina_has_Zgloszenie (Dziedzina_idDziedzina);
CREATE INDEX IFK_Zgloszenie_Dziedzina_has ON Dziedzina_has_Zgloszenie (Zgloszenie_idZgloszenie);


CREATE TABLE Naprawa (
  Zgloszenie_idZgloszenie INTEGER   NOT NULL ,
  Specjalista_idSpecjalista INTEGER   NOT NULL   ,
PRIMARY KEY(Zgloszenie_idZgloszenie, Specjalista_idSpecjalista)    ,
  FOREIGN KEY(Zgloszenie_idZgloszenie)
    REFERENCES Zgloszenie(idZgloszenie),
  FOREIGN KEY(Specjalista_idSpecjalista)
    REFERENCES Specjalista(idSpecjalista));


CREATE INDEX Zgloszenie_has_Specjalista_FKIndex1Naprawa ON Naprawa (Zgloszenie_idZgloszenie);
CREATE INDEX Zgloszenie_has_Specjalista_FKIndex2Naprawa ON Naprawa (Specjalista_idSpecjalista);


CREATE INDEX IFK_Zgloszenie_Naprawa ON Naprawa (Zgloszenie_idZgloszenie);
CREATE INDEX IFK_Specjalista_Naprawa ON Naprawa (Specjalista_idSpecjalista);


-------------------------------------------logika biznesowa


----------dodanie brakujacych w postgresql funkcji agregujących

CREATE OR REPLACE FUNCTION public.first_agg ( anyelement, anyelement )
RETURNS anyelement LANGUAGE sql IMMUTABLE STRICT AS $$
        SELECT $1;
$$;
 
CREATE AGGREGATE public.first (
        sfunc    = public.first_agg,
        basetype = anyelement,
        stype    = anyelement
);


CREATE OR REPLACE FUNCTION public.last_agg ( anyelement, anyelement )
RETURNS anyelement LANGUAGE sql IMMUTABLE STRICT AS $$
        SELECT $2;
$$;
 
CREATE AGGREGATE public.last (
        sfunc    = public.last_agg,
        basetype = anyelement,
        stype    = anyelement
);



----------------wypełnienie tablicy uprawnień

INSERT INTO uprawnienia VALUES 
  (DEFAULT, 'Pracownik', 'Typowy szary pracownik przeszkadzający adminom w funkcjonowaniu systemu.' ),
  (DEFAULT, 'Specjalista', 'Specjalista gaszący pożary tworzone przez użytkowników i zwierzchników.' ),
  (DEFAULT, 'Szef', 'Człowiek niańczący i używający wszystkich możliwych sztuczek żeby zmusić podwładnych do pracy.' ),
  (DEFAULT, 'Admin', 'Pierwszy po bogu, trzymający cały cyrk w całośći.' )
    ;
-------------------wypełnienie tablicy stanu zgłoszeń

INSERT INTO stan_zgloszenia VALUES 
  (DEFAULT, 'Oczekujący'),
  (DEFAULT, 'Naprawiany'),
  (DEFAULT, 'Sprawdzany'),
  (DEFAULT, 'Sprawdzony'),
  (DEFAULT, 'Rozwiązany'),
  (DEFAULT, 'Odrzucony')
    ;

-------------------wypełnienie tablicy dziedzin


    
 
INSERT INTO Dziedzina VALUES 
  (DEFAULT, 'Elektryka'),
  (DEFAULT, 'Elektronika'),
  (DEFAULT, 'Problem programistyczny'),
  (DEFAULT, 'Brudny stół'),
  (DEFAULT, 'Ćały otaczający świat'),
  (DEFAULT, 'Interacje z ludźmi')
  ;
  
  
  
-----------------------widoki

-----------------dane szefów
CREATE OR REPLACE VIEW PracownicySzefow AS
SELECT Up.Pracownik_idPracownik AS szef, Specjalista.Pracownik_idPracownik AS jego_specjalista, Specjalista.idSpecjalista AS id_Specjalista  
					    FROM Specjalista FULL OUTER JOIN (
							  SELECT * FROM Pracownik_has_Uprawnienia WHERE Uprawnienia_idUprawnienia = 3 ) as Up
					ON Specjalista.Szef_idPracownik = Up.Pracownik_idPracownik
					;
					
					

CREATE OR REPLACE VIEW Obciazenie_szefow AS
SELECT szef, COUNT(jego_specjalista) AS obciazenie_specjalistow FROM PracownicySzefow LEFT JOIN 
							    (SELECT * FROM Naprawa UNION ALL SELECT * FROM Sprawdzenie) AS wszyscy 
							      ON PracownicySzefow.id_Specjalista = wszyscy.specjalista_idspecjalista
GROUP BY szef
;


CREATE OR REPLACE VIEW DziedzinySzefow AS
SELECT  DISTINCT Dziedzina_idDziedzina AS dziedzinaSzefa , szef  FROM PracownicySzefow FULL OUTER JOIN Dziedzina_has_Specjalista ON PracownicySzefow.id_Specjalista = Dziedzina_has_Specjalista.Specjalista_idSpecjalista
ORDER BY szef;
  
  
  
  --Zgloszenie
CREATE OR REPLACE VIEW
Zgloszenie_full
AS SELECT 
Zgloszenie.idzgloszenie AS nr, 
Zgloszenie.pracownik_idpracownik AS pracownik,
Zgloszenie.szef_idpracownik AS szef, 
Dziedzina.opisDziedzina,
Zgloszenie.opis ,
Zgloszenie.data_zgloszenia ,
Zgloszenie.data_zamkniecia ,
stan_zgloszenia.stan,
Zgloszenie.stan_zgloszenia_idstan_zgloszenia AS nr_stan
FROM Zgloszenie, stan_zgloszenia , Dziedzina_has_Zgloszenie, Dziedzina
WHERE Zgloszenie.stan_zgloszenia_idstan_zgloszenia = stan_zgloszenia.idstan_zgloszenia AND
Zgloszenie.idzgloszenie = Dziedzina_has_Zgloszenie.Zgloszenie_idZgloszenie AND
Dziedzina.idDziedzina= Dziedzina_has_Zgloszenie.Dziedzina_idDziedzina
;
---------------------------------zapewnia odseparowanie haseł
CREATE OR REPLACE VIEW
pracownikSC
AS
SELECT  idpracownik ,   imie   ,   nazwisko   ,    email    ,  telefon   ,  posada   , login  FROM pracownik;  
  


----------------triger doający automatycznie uprawnienia pracownikowi

CREATE OR REPLACE FUNCTION dodaj_pracownikaUprawnieniaFUNC() RETURNS TRIGGER AS'
    DECLARE
      uprawn VARCHAR(15);
      
      result text;
    BEGIN
	uprawn := tg_argv[0];
	IF uprawn = ''Specjalista'' THEN
	  RAISE NOTICE ''Nie można dodać SPECJALISTY w ten sposób'';
	  RETURN NULL;
	ELSE
	  SELECT dodaj_UprawnieniaFUNC(NEW.login, uprawn) INTO result; 
	  RETURN NEW;
        END IF;
    END;'
 LANGUAGE 'plpgsql';



---uprawnienia
 
CREATE OR REPLACE FUNCTION dodaj_UprawnieniaFUNC(VARCHAR(10), VARCHAR(15))
RETURNS VOID AS'
DECLARE
      loginUp ALIAS FOR $1;
      uprawn ALIAS FOR $2;
      BEGIN
  
	INSERT INTO pracownik_has_uprawnienia VALUES (
	(SELECT idUprawnienia FROM uprawnienia WHERE nazwa = uprawn), 
	(SELECT idpracownik FROM pracownik WHERE login = loginUp)
	);
	
	
	RAISE NOTICE ''Nadano  % % uprawnienia %'', 
	(SELECT imie FROM pracownik WHERE login = loginUp),
	(SELECT nazwisko FROM pracownik WHERE login = loginUp),
	(SELECT nazwa FROM uprawnienia WHERE nazwa = uprawn);
							  
        RETURN;
    END;'
 LANGUAGE 'plpgsql'; 
 
 CREATE OR REPLACE FUNCTION dodaj_UprawnieniaFUNC(integer, integer)
RETURNS VOID AS'
DECLARE
      pracownikID ALIAS FOR $1;
      UprawnieniaID ALIAS FOR $2;
      BEGIN
  
	INSERT INTO pracownik_has_uprawnienia VALUES (
	(UprawnieniaID), 
	(pracownikID)
	);
	
	
	RAISE NOTICE ''Nadano  % % uprawnienia %'', 
	(SELECT imie FROM pracownik WHERE idpracownik = pracownikID),
	(SELECT nazwisko FROM pracownik WHERE idpracownik = pracownikID),
	(SELECT nazwa FROM uprawnienia WHERE idUprawnienia = UprawnieniaID);
							  
        RETURN;
    END;'
 LANGUAGE 'plpgsql';  
 
 -------------------podstawowym uprawnieiem jest pracownik
CREATE TRIGGER dodaj_pracownikaUprawnieniaFUNC AFTER INSERT ON pracownik
FOR EACH ROW EXECUTE PROCEDURE dodaj_pracownikaUprawnieniaFUNC('Pracownik');


----------------------------------------triger usuwanie pracownika

CREATE OR REPLACE FUNCTION usun_pracownikaUprawnieniaFUNC() RETURNS TRIGGER AS'
    BEGIN
	IF EXISTS (SELECT * FROM specjalista WHERE pracownik_idpracownik=OLD.idpracownik)
	THEN
	    RAISE NOTICE ''USUWAM SPECJALISTE  % % '', old.imie, old.nazwisko;

	    DELETE FROM specjalista WHERE pracownik_idpracownik=OLD.idpracownik;
	
	
	END IF;



	DELETE FROM pracownik_has_uprawnienia WHERE pracownik_idpracownik = old.idpracownik;
	
	RAISE NOTICE ''Usunięto uprawnieia  % % '', old.imie, old.nazwisko;

        RETURN OLD;
    END;'
 LANGUAGE 'plpgsql';

CREATE TRIGGER usun_pracownikaUprawnieniaFUNC BEFORE DELETE ON pracownik
FOR EACH ROW EXECUTE PROCEDURE usun_pracownikaUprawnieniaFUNC();
 
 
------------------------dodatkowy triger do usunięcia specjalisy 
 
CREATE OR REPLACE FUNCTION usun_specjalisteUprawnieniaFUNC() RETURNS TRIGGER AS'
    BEGIN

	DELETE FROM dziedzina_has_specjalista WHERE
	specjalista_idspecjalista = OLD.idspecjalista;
	DELETE FROM naprawa WHERE specjalista_idspecjalista = OLD.idspecjalista;
	DELETE FROM sprawdzenie WHERE specjalista_idspecjalista = OLD.idspecjalista;

	
	
	RAISE NOTICE ''USUWAM SPECJALISTE o numerze % '', old.idspecjalista;
	
	

        RETURN OLD;
    END;'
 LANGUAGE 'plpgsql';



CREATE TRIGGER usun_specjalisteUprawnieniaFUNC BEFORE DELETE ON specjalista
FOR EACH ROW EXECUTE PROCEDURE usun_specjalisteUprawnieniaFUNC();


------------------------------------dodanie przykładowych pracowników
INSERT INTO pracownik VALUES (DEFAULT, 'Adam', 'Kowalski', 'ak@email.pl','7000855468','cieć','ak','ak');
INSERT INTO pracownik VALUES (DEFAULT, 'Alfons', 'Mak', 'am@email.pl','7000855468','konserwator','am','am');

INSERT INTO pracownik VALUES (DEFAULT, 'Ada', 'Lak', 'aL@email.pl','7000855468','tester wykałaczek','al','al');
INSERT INTO pracownik VALUES
    (DEFAULT, 'Konrad',' Sierotowicz', 'ks@email.pl','7000855468','tester wykałaczek','ks','ks'),
    (DEFAULT, 'Olga', 'Martiszek', 'om@email.pl','7000855468','tester wykałaczek','om','om'),
    (DEFAULT, 'Ewa', 'Łączkowska', 'el@email.pl','7000855468','tester wykałaczek','el','el'),
    (DEFAULT, 'Grzesiek', 'Brdej', 'gb@email.pl','7000855468','tester wykałaczek','gb','gb'),
    (DEFAULT, 'Justyna', 'Olesiak', 'jo@email.pl','7000855468','tester wykałaczek','jo','jo'),
    (DEFAULT, 'Sabina', 'Szkarłat', 'ss@email.pl','7000855468','tester wykałaczek','ss','ss'),
    (DEFAULT, 'Łukasz', 'Kamiński', 'lk@email.pl','7000855468','tester wykałaczek','lk','lk'),
    (DEFAULT, 'Jakub', 'Sęk', 'js@email.pl','7000855468','tester wykałaczek','js','js'),
    (DEFAULT, 'Beata', 'Winiarska', 'bw@email.pl','7000855468','tester wykałaczek','bw','bw'),
    (DEFAULT, 'Paweł', 'Jaworski', 'pj@email.pl','7000855468','tester wykałaczek','pj','pj'),
    (DEFAULT, 'Dariusz', 'Zaczyk', 'dz@email.pl','7000855468','tester wykałaczek','dz','dz')
    ;

-------------------------------funkcja dodająca specjalistę    
CREATE OR REPLACE FUNCTION dodaj_SpecjalisteFUNC(INTEGER, INTEGER,INTEGER)
RETURNS VOID AS'
DECLARE
      pracownikID ALIAS FOR $1;
      szefID ALIAS FOR $2;
      dziedzinaID ALIAS FOR $3;
      result text;
      jestSzef BOOLEAN;
      inntSzef BOOLEAN;
      BEGIN
	jestSzef := (EXISTS (SELECT pracownik_idpracownik FROM pracownik_has_uprawnienia WHERE
	pracownik_idpracownik = szefID AND uprawnienia_iduprawnienia=3));
	inntSzef := NOT (  EXISTS ( SELECT pracownik_idpracownik FROM pracownik_has_uprawnienia WHERE
	pracownik_idpracownik = pracownikID AND uprawnienia_iduprawnienia=3  
	)	 AND pracownikID<>szefID);
      
	IF jestSzef AND inntSzef
	
	THEN
	   
	  INSERT INTO specjalista VALUES ( 
	  DEFAULT, pracownikID, szefID);
	  INSERT INTO public.dziedzina_has_specjalista VALUES
	  (dziedzinaID,
	  (SELECT idspecjalista FROM specjalista WHERE pracownik_idpracownik = pracownikID)
	  );
	
	  SELECT dodaj_UprawnieniaFUNC((SELECT login FROM pracownik WHERE idpracownik = pracownikID),''Specjalista'') into result;
	  
	  
	  RAISE NOTICE '' % % jest specjalistą  % %'', 
	  (SELECT imie FROM pracownik WHERE idpracownik = pracownikID),
	  (SELECT nazwisko FROM pracownik WHERE idpracownik = pracownikID),
	  (SELECT imie FROM pracownik WHERE idpracownik = szefID),
	  (SELECT nazwisko FROM pracownik WHERE idpracownik = szefID);
							    
	  RETURN;
	ELSE
	    IF NOT jestSzef THEN
	    RAISE NOTICE ''brak szefa   % % '',
	    (SELECT imie FROM pracownik WHERE idpracownik=szefID),
	    (SELECT nazwisko FROM pracownik WHERE idpracownik=szefID);
	    ELSE
	    
	    RAISE NOTICE ''  % % jest szefem i nie może mieć specjalisty '',
	    (SELECT imie FROM pracownik WHERE idpracownik=szefID),
	    (SELECT nazwisko FROM pracownik WHERE idpracownik=szefID);
	    
	    
	    END IF;
	    RETURN;
	END IF;
    END;'
 LANGUAGE 'plpgsql'; 
 
 
 ----------------------------------------triger zabraniający usunięcia ostatniego admina co zablokuje zarządzanie bazą
 CREATE OR REPLACE FUNCTION zachowajJednego() RETURNS TRIGGER AS'
    DECLARE
    delID INTEGER;
    BEGIN
	delID := tg_argv[0];
	
	IF NOT EXISTS (SELECT * FROM pracownik_has_uprawnienia WHERE uprawnienia_iduprawnienia = 4 AND pracownik_idpracownik <> OLD.pracownik_idpracownik )
	THEN
	    RAISE NOTICE ''NIE MOŻESZ USUNĄĆ OSTATNIEGO   %'', (SELECT nazwa FROM uprawnienia WHERE iduprawnienia = delID ) ;
	    
	    RETURN NULL;
	ELSE
	    RETURN OLD;
	END IF;
    END;'
 LANGUAGE 'plpgsql';


CREATE TRIGGER zachowajJednegoAdmina BEFORE DELETE ON pracownik_has_uprawnienia FOR EACH ROW
EXECUTE PROCEDURE zachowajJednego(4);


-----------------------------------------------zabezpieczenie przed niepoprawny usunięciem specjalisty
CREATE OR REPLACE FUNCTION nie_wolno_wyrzucic_specjalisty_z_uprawnienF() RETURNS TRIGGER AS'
    DECLARE
    delID INTEGER;
    BEGIN
	delID := tg_argv[0];
	
	IF ((EXISTS (SELECT * FROM specjalista WHERE  pracownik_idpracownik = OLD.pracownik_idpracownik ))
	  and old.uprawnienia_iduprawnienia=2)
	THEN
	    RAISE NOTICE ''Najpierw usuń z tablicy specjalista '';
	    
	    RETURN NULL;
	ELSE
	    RETURN OLD;
	END IF;
    END;'
 LANGUAGE 'plpgsql';


CREATE TRIGGER nie_wolno_wyrzucic_specjalisty_z_uprawnien BEFORE DELETE ON pracownik_has_uprawnienia FOR EACH ROW
EXECUTE PROCEDURE nie_wolno_wyrzucic_specjalisty_z_uprawnienF();







-----------------------------------triger zabezpieczający tablice słownikowe przed edytowaniem

CREATE OR REPLACE FUNCTION blokowanie_tablicyF() RETURNS TRIGGER AS'
    BEGIN
      IF tg_argv[0] THEN
      RAISE NOTICE ''Nie wolno edytować tablicy %'',TG_TABLE_NAME;
 
      RETURN NULL;
      ELSE
      RAISE NOTICE ''Zaleca się ostrożne edytowanie tablicy %'',TG_TABLE_NAME;
 
      RETURN OLD;
      END IF;
    END;'
 LANGUAGE 'plpgsql';


CREATE TRIGGER blokowanie_tablicy BEFORE DELETE OR INSERT OR UPDATE ON uprawnienia FOR EACH ROW
EXECUTE PROCEDURE blokowanie_tablicyF(TRUE);

CREATE TRIGGER blokowanie_tablicy BEFORE DELETE OR INSERT OR UPDATE ON stan_zgloszenia FOR EACH ROW
EXECUTE PROCEDURE blokowanie_tablicyF(TRUE);

CREATE TRIGGER blokowanie_tablicy BEFORE DELETE OR INSERT OR UPDATE ON dziedzina FOR EACH ROW
EXECUTE PROCEDURE blokowanie_tablicyF(FALSE);

/*
INSERT INTO uprawnienia VALUES 
  (DEFAULT, 'dsfdfsdfsd', 'Typowy szary pracownik przeszkadzający adminom w funkcjonowaniu systemu.' );



DELETE FROM uprawnienia where iduprawnienia=1;

DELETE FROM dziedzina where idDziedzina=6;

select * from uprawnienia;
*/

----------------------------triger zapewniający różnych specjalistów sprawdzających i naprawiających zgłoszenie
CREATE OR REPLACE FUNCTION specjalista_nie_moze_sie_sprawdzacF() RETURNS TRIGGER AS'
    DECLARE
    tablica text;
    BEGIN 	
	IF (TG_RELNAME=''naprawa'') THEN
	
	    IF EXISTS (SELECT * FROM sprawdzenie WHERE
	    specjalista_idspecjalista=NEW.specjalista_idspecjalista AND
	    zgloszenie_idzgloszenie=NEW.zgloszenie_idzgloszenie) THEN
	    
	      RAISE NOTICE ''Specjalista juz w tablicy sprawdzenie '';	    
	      RETURN NULL;
	    ELSE
	      RETURN NEW;
	    END IF;
	ELSIF (TG_RELNAME=''sprawdzenie'') THEN
	
	    IF EXISTS (SELECT * FROM naprawa WHERE
	    specjalista_idspecjalista=NEW.specjalista_idspecjalista AND
	    zgloszenie_idzgloszenie=NEW.zgloszenie_idzgloszenie) THEN
	    
	      RAISE NOTICE ''Specjalista juz w tablicy naprawa '';	    
	      RETURN NULL;
	    ELSE
	      RETURN NEW;
	    END IF;
	END IF;
    END;'
 LANGUAGE 'plpgsql';


CREATE TRIGGER specjalista_nie_moze_sie_sprawdzac BEFORE INSERT ON sprawdzenie FOR EACH ROW
EXECUTE PROCEDURE specjalista_nie_moze_sie_sprawdzacF();

CREATE TRIGGER specjalista_nie_moze_sie_naprawiac BEFORE INSERT ON naprawa FOR EACH ROW
EXECUTE PROCEDURE specjalista_nie_moze_sie_sprawdzacF();





---------------------------------dodawanie zgłoszenia
CREATE OR REPLACE FUNCTION dodaj_Zgloszenie(INTEGER, INTEGER,VARCHAR(255),INTEGER)
RETURNS VOID AS'
DECLARE
      pracownikID ALIAS FOR $1;
      szefID ALIAS FOR $2;
      opis ALIAS FOR $3;
      dziedzinaID ALIAS FOR $4;
      result text;
      BEGIN
	IF ( szefID IS NULL) THEN
	  
	    
	    
	    szefID :=(SELECT first(Obciazenie_szefow.szef) as najmniej_obciazony_szef FROM Obciazenie_szefow 
	    WHERE obciazenie_specjalistow = (
		SELECT MIN(obciazenie_specjalistow) FROM Obciazenie_szefow WHERE Obciazenie_szefow.szef IN(
		SELECT DziedzinySzefow.szef FROM DziedzinySzefow WHERE DziedzinySzefow.dziedzinaSzefa IN
		(SELECT Dziedzina_idDziedzina FROM Dziedzina_has_Zgloszenie WHERE Zgloszenie_idZgloszenie = 1))));
		
	    RAISE NOTICE ''AUTOMATYCZNE DODANIE SZEFA % '', szefID;
		
   
	    
	    
	    
	
	END IF;
		
	
	INSERT INTO Zgloszenie VALUES  (DEFAULT, pracownikID, szefID, 1 ,opis, DEFAULT, NULL);
	INSERT INTO Dziedzina_has_Zgloszenie (zgloszenie_idzgloszenie, dziedzina_iddziedzina) VALUES  (
						      (SELECT LAST(idzgloszenie) FROM Zgloszenie), dziedzinaID
						      );
						      

      END;'
 LANGUAGE 'plpgsql';

 
 
 /*
 
INSERT INTO Zgloszenie VALUES  (DEFAULT, pracownikID, szefID, 1 ,opis, DEFAULT, NULL);
	INSERT INTO Dziedzina_has_Zgloszenie VALUES  (
						      (SELECT LAST(idzgloszenie) FROM Zgloszenie), dziedzinaID
						      );

*/
--------------------------------------------------------usuwanie szefa
---------------------------------- jest wyszukiwany najmniej_obciazony_szef i on dziedziczy pracowników i zlecenia szef
CREATE OR REPLACE FUNCTION usun_szefaF() RETURNS TRIGGER AS'
    DECLARE
      oldSzef INTEGER; 
      newSzef INTEGER;
    BEGIN
	IF (OLD.Uprawnienia_idUprawnienia = 3) THEN
	oldSzef := OLD.Pracownik_idPracownik;
	newSzef :=(SELECT first(Obciazenie_szefow.szef) FROM Obciazenie_szefow 
		    WHERE  szef <> oldSzef
		    AND obciazenie_specjalistow = (
		SELECT MIN(obciazenie_specjalistow) FROM Obciazenie_szefow));
	UPDATE Zgloszenie SET Szef_idPracownik=newSzef WHERE  Szef_idPracownik=oldSzef;
	UPDATE Specjalista SET Szef_idPracownik=newSzef WHERE  Szef_idPracownik=oldSzef;
	
	RAISE NOTICE ''Własności szefa % Przejmuje szef % '', oldSzef, newSzef;
	
	END IF;
	
	
	RETURN OLD;
    END;'
 LANGUAGE 'plpgsql';



CREATE TRIGGER usun_szefa BEFORE DELETE ON Pracownik_has_Uprawnienia
FOR EACH ROW EXECUTE PROCEDURE usun_szefaF();

/*
CREATE OR REPLACE FUNCTION kolejnosc_dat_zgloszenieF() RETURNS TRIGGER AS'
    DECLARE
    BEGIN
	IF (OLD.data_zgloszenia > NEW.data_zamkniecia) THEN
	
	  RAISE NOTICE ''data zamknięcia % jest wcześniejesza niż data zgłoszenia % '', NEW.data_zamkniecia, OLD.data_zgloszenia;
	  RETURN NULL;
	ELSE
	  RETURN NEW;
	END IF;
    END;'
 LANGUAGE 'plpgsql';



CREATE TRIGGER kolejnosc_dat_zgloszenie BEFORE UPDATE ON zgloszenie
FOR EACH ROW EXECUTE PROCEDURE kolejnosc_dat_zgloszenieF();
*/



CREATE OR REPLACE FUNCTION zmien_stan(INTEGER, INTEGER, INTEGER)
RETURNS VOID AS'
DECLARE
      personID ALIAS FOR $1;
      zgloszenieID ALIAS FOR $2;
      value ALIAS FOR $3;
      BEGIN
      
	IF (personID = (SELECT Szef_idPracownik FROM Zgloszenie WHERE idZgloszenie = zgloszenieID AND stan_zgloszenia_idstan_zgloszenia in (1,4) ) AND (value in (2,5,6))) OR
	(((SELECT idSpecjalista FROM Specjalista WHERE Pracownik_idPracownik = personID) in(  
	(SELECT specjalista_idspecjalista FROM Naprawa WHERE Zgloszenie_idZgloszenie = zgloszenieID ) )
	)  AND
	(value = 3) AND (2 = (SELECT stan_zgloszenia_idstan_zgloszenia FROM Zgloszenie WHERE idZgloszenie = zgloszenieID ))) OR
	
	(((SELECT idSpecjalista FROM Specjalista WHERE Pracownik_idPracownik = personID) in(  (SELECT specjalista_idspecjalista FROM Sprawdzenie WHERE Zgloszenie_idZgloszenie = zgloszenieID ) ))  AND (value = 4) AND (3 = (SELECT stan_zgloszenia_idstan_zgloszenia FROM Zgloszenie WHERE idZgloszenie = zgloszenieID ))) 
	THEN
	  UPDATE Zgloszenie SET stan_zgloszenia_idstan_zgloszenia = value WHERE idZgloszenie = zgloszenieID;
	  RAISE NOTICE ''Dziala dla % w zgloszeniu % watość %'',  
					    personID,
					    zgloszenieID ,
					    value ;

	ELSE
	
	  RAISE NOTICE '' %  % nie może zmienić własności zgłoszenia %  na % '', (SELECT imie FROM pracownik WHERE idPracownik = personID) ,
										(SELECT nazwisko FROM pracownik WHERE idPracownik = personID) ,
										(zgloszenieID),
										(SELECT stan FROM stan_zgloszenia WHERE idstan_zgloszenia = value) ;    
        RETURN;
        END IF;
    END;'
 LANGUAGE 'plpgsql'; 




 
/* 
 CREATE OR REPLACE FUNCTION zamknij_dataF()
RETURNS TRIGGER AS'
DECLARE
      BEGIN
      
	IF ((new.stan_zgloszenia_idstan_zgloszenia in(4,5)) AND (new.data_zamkniecia IS NULL)  ) THEN	  
	 RAISE NOTICE ''Dzial'';

	   RETURN NEW;
        ELSE
	    RETURN NULL;
        END IF;
        
    END;'
 LANGUAGE 'plpgsql'; 
 
 



CREATE TRIGGER zamknij_data BEFORE UPDATE ON zgloszenie
FOR EACH ROW EXECUTE PROCEDURE zamknij_dataF();

*/


CREATE OR REPLACE FUNCTION zakoncz(INTEGER, INTEGER,INTEGER)
RETURNS VOID AS'
DECLARE
      personID ALIAS FOR $1;
      zgloszenieID ALIAS FOR $2;
      value ALIAS FOR $3;
      result text;
    BEGIN
      SELECT zmien_stan(personID, zgloszenieID, value) INTO result;
      UPDATE Zgloszenie SET  data_zamkniecia = DEFAULT WHERE idZgloszenie = zgloszenieID;
     RETURN;
    END;'
 LANGUAGE 'plpgsql'; 
















-----------------------------------------dodanie innych przypadków

SELECT dodaj_UprawnieniaFUNC((SELECT login FROM pracownik WHERE imie='Konrad' ),'Admin');
SELECT dodaj_UprawnieniaFUNC((SELECT login FROM pracownik WHERE imie='Adam' ),'Admin');

------------------------------------ dodanie szefów
SELECT dodaj_UprawnieniaFUNC((SELECT login FROM pracownik WHERE idpracownik=1 ),'Szef');
SELECT dodaj_UprawnieniaFUNC((SELECT login FROM pracownik WHERE idpracownik=2 ),'Szef');
SELECT dodaj_UprawnieniaFUNC((SELECT login FROM pracownik WHERE idpracownik=3 ),'Szef');



------------------------------------ dodanie specjalistów
/*
dopisać spawdzenie szefa
szef nie może być szefem innego szefa
pracownik 
*/


SELECT dodaj_SpecjalisteFUNC(4, 1,1);
SELECT dodaj_SpecjalisteFUNC(7, 1,1);
SELECT dodaj_SpecjalisteFUNC(8, 1,1);
SELECT dodaj_SpecjalisteFUNC(1, 1,1);

 
SELECT dodaj_Zgloszenie( 1, 2,'nie działa',1);
SELECT  dodaj_Zgloszenie( 2, 1,'nie działa też',2);
SELECT  dodaj_Zgloszenie( 2, NULL,'nie działa też',3);
----------------------łączenie specjalistów ze zgtłoszeniami
INSERT INTO sprawdzenie VALUES
  (1,1),
  (2,1)  
  ;
INSERT INTO naprawa VALUES
  (1,2),
  (2,2),
  (3,2);
UPDATE zgloszenie SET data_zamkniecia = default where idzgloszenie=1;


------------------------------przykładowe raporty  itp.

/*
----------------------------raport dla przykładowego pracownika

select NULL as raport_pracownika_zgloszenia_otwarte;
SELECT nr, Pracownik.imie as oddelegowany, Pracownik.nazwisko as szef, stan , opisDziedzina as dziedzna, data_zgloszenia, opis  FROM Zgloszenie_full, Pracownik WHERE
pracownik = 2 AND data_zamkniecia IS NULL
AND Pracownik.idPracownik = Zgloszenie_full.szef
ORDER BY data_zgloszenia ;
;

------------------------------zgłoszenia zamkniete
select NULL as raport_pracownika_zgloszenia_otwarte;
SELECT * FROM Zgloszenie_full WHERE
pracownik = 1 AND data_zamkniecia IS NOT NULL
ORDER BY data_zgloszenia;
;



----------------------------raport dla przykładowego specjalisty
--id specjalisty 2

select NULL as raport_Specjalisty_naprawa;

SELECT
distinct Zgloszenie_full.nr,
pracownik.imie,
pracownik.nazwisko,
Zgloszenie_full.stan,
Zgloszenie_full.opisDziedzina,
Zgloszenie_full.opis
FROM pracownik, Zgloszenie_full, Naprawa
WHERE 
pracownik.idPracownik = Zgloszenie_full.pracownik AND
Zgloszenie_full.nr = Naprawa.Zgloszenie_idZgloszenie AND
Naprawa.Specjalista_idSpecjalista = 2 AND 
Zgloszenie_full.data_zamkniecia IS NULL
;

-- specjalizacje specjalisty

INSERT INTO dziedzina_has_specjalista VALUES (2,2);


select NULL as specjalizacje_specjalisty;


SELECT Dziedzina.opisdziedzina AS Specjalnosc FROM Dziedzina_has_Specjalista JOIN Dziedzina ON Dziedzina.idDziedzina=Dziedzina_has_Specjalista.Dziedzina_idDziedzina
WHERE Dziedzina_has_Specjalista.Specjalista_idSpecjalista=2;


-----------------------SZEF

SELECT * FROM PracownicySzefow WHERE szef = 1;
SELECT * FROM obciazenie_szefow WHERE szef = 1;
SELECT * FROM dziedzinyszefow WHERE szef = 1;


SELECT
distinct Zgloszenie_full.nr,
pracownik.imie,
pracownik.nazwisko,
Zgloszenie_full.stan,
Zgloszenie_full.opisDziedzina,
Zgloszenie_full.opis,
Zgloszenie_full.data_zgloszenia
FROM pracownik, Zgloszenie_full
WHERE 
pracownik.idPracownik = Zgloszenie_full.pracownik AND
Zgloszenie_full.szef =  1 AND
Zgloszenie_full.data_zamkniecia IS NOT NULL
ORDER BY data_zgloszenia
;

---------------------------------ADmin
-----------------wszyscy np specjliści


SELECT * FROM pracownikSC WHERE idPracownik IN(SELECT Pracownik_idPracownik FROM Specjalista)
;
-------------dostępne specjalizacje
SELECT Dziedzina.opisDziedzina AS dostepne_specjalnosci, COUNT(Dziedzina_has_Specjalista.Specjalista_idSpecjalista) as ludzi FROM Dziedzina_has_Specjalista, Dziedzina WHERE Dziedzina.idDziedzina=Dziedzina_has_Specjalista.Dziedzina_idDziedzina
GROUP BY Dziedzina.opisDziedzina
;
------------- brakujące specjalizacje
SELECT DISTINCT Dziedzina.opisDziedzina AS brakujace_specjalnosci FROM Dziedzina WHERE idDziedzina NOT IN (
	    SELECT Dziedzina_idDziedzina FROM Dziedzina_has_Specjalista
	    );
;
SELECT Pracownik.imie as Imie, Pracownik.nazwisko AS Nazwisko, Obciazenie_szefow.obciazenie_specjalistow AS Obciazenie  FROM  Obciazenie_szefow JOIN Pracownik ON Pracownik.idPracownik=Obciazenie_szefow.szef;


SELECT DziedzinySzefow.szef, Dziedzina.opisdziedzina FROM DziedzinySzefow LEFT JOIN Dziedzina ON DziedzinySzefow.dziedzinaszefa=Dziedzina.idDziedzina ;


*/