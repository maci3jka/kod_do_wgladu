# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'loginWindow.ui'
#
# Created: Sun Jan 11 20:51:04 2015
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_LOGIN(object):
    def setupUi(self, LOGIN):
        LOGIN.setObjectName("LOGIN")
        LOGIN.resize(254, 178)
        self.buttonBox = QtGui.QDialogButtonBox(LOGIN)
        self.buttonBox.setGeometry(QtCore.QRect(40, 130, 171, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayoutWidget = QtGui.QWidget(LOGIN)
        self.formLayoutWidget.setGeometry(QtCore.QRect(10, 30, 231, 81))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.lineEditUzytkownik = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEditUzytkownik.setObjectName("lineEditUzytkownik")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditUzytkownik)
        self.labeUzytkownik = QtGui.QLabel(self.formLayoutWidget)
        self.labeUzytkownik.setObjectName("labeUzytkownik")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labeUzytkownik)
        self.labelHaslo = QtGui.QLabel(self.formLayoutWidget)
        self.labelHaslo.setObjectName("labelHaslo")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelHaslo)
        self.lineEditHaslo = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEditHaslo.setObjectName("lineEditHaslo")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditHaslo)
        self.comboBox = QtGui.QComboBox(self.formLayoutWidget)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.comboBox)

        self.retranslateUi(LOGIN)
        #QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), LOGIN.accept)
        #QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), LOGIN.reject)
        #QtCore.QMetaObject.connectSlotsByName(LOGIN)

    def retranslateUi(self, LOGIN):
        LOGIN.setWindowTitle(QtGui.QApplication.translate("LOGIN", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.labeUzytkownik.setText(QtGui.QApplication.translate("LOGIN", "Użytkownik", None, QtGui.QApplication.UnicodeUTF8))
        self.labelHaslo.setText(QtGui.QApplication.translate("LOGIN", "Hasło", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox.setItemText(0, QtGui.QApplication.translate("LOGIN", "Pracownik", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox.setItemText(1, QtGui.QApplication.translate("LOGIN", "Specjalista", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox.setItemText(2, QtGui.QApplication.translate("LOGIN", "Szef", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox.setItemText(3, QtGui.QApplication.translate("LOGIN", "Administrator", None, QtGui.QApplication.UnicodeUTF8))

