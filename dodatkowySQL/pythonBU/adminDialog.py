# -*- coding: utf-8 -*-
__author__ = 'maciek'

from PySide.QtGui import *
from PySide.QtCore import *
from showTable import *
from funkcje import *

import adminMainWindow
from dbError import *

from funkcje import *

class AdminDialog(QDialog, adminMainWindow.Ui_DialogAdmin):
    def __init__(self,userid, parent=None):
        super(AdminDialog, self).__init__(parent)
        self.setupUi(self)
        self.userid=userid
        self.retranslateUi2()
######## zakladka raport
        self.connect(self.pushButton_zamknij, SIGNAL("clicked()"), quit)
        self.connect(self.pushButton_wyloguj, SIGNAL("clicked()"), self.close)
        # self.connect()
        # self.pushButtonR_wszyscy.clicked.connect(self.wszyscy)
        self.connect(self.pushButtonR_wszyscy, SIGNAL("clicked()"), self.wszyscy)
        self.connect(self.pushButtonR_specjalici, SIGNAL("clicked()"), self.specjalisci)
        self.connect(self.pushButtonR_szefowie, SIGNAL("clicked()"), self.szefowie)
        self.connect(self.pushButtonR_admini, SIGNAL("clicked()"), self.admini)
        self.connect(self.pushButtonR_specjalizacje, SIGNAL("clicked()"), self.specjalizacje)
        self.connect(self.pushButtonR_braki, SIGNAL("clicked()"), self.braki)
        self.connect(self.pushButtonR_obciazerie, SIGNAL("clicked()"), self.obciazenie)
######### zakladka dodaj
    #########pracownika


        self.connect(self.pushButton_Dodaj_prac, SIGNAL("clicked()"), self.dodPrac)
        self.lineEdit_haslo.setEchoMode(QLineEdit.Password )
    #########specjaliste
        self.connect(self.pushButton_Dodaj_prac_2, SIGNAL("clicked()"), self.dodSpec)
        self.connect(self.pushButton_Dodaj_prac_3, SIGNAL("clicked()"), self.dodSpec)


        self.connect(self.pushButton_2, SIGNAL("clicked()"), self.pokPracNsp)
        self.crPrac=None
        self.connect(self.pushButton_3, SIGNAL("clicked()"), self.pokSzef)

        self.crSzef=None

    #########szefa i admina
        self.connect(self.pushButton_4, SIGNAL("clicked()"), self.pokPracNsz)
        self.connect(self.pushButton_6, SIGNAL("clicked()"), self.pokPracNad)
        self.connect(self.pushButton_Dodaj_prac_3, SIGNAL("clicked()"), self.dodSze)
        self.connect(self.pushButton_Dodaj_prac_4, SIGNAL("clicked()"), self.dodAdm)

######### zakladka dodaj
    #########pracownika
        self.connect(self.pushButton_11, SIGNAL("clicked()"), self.pokPra)
        self.connect(self.pushButton_usunj_prac, SIGNAL("clicked()"), self.usuPrac)

    #########specjaliste
        self.connect(self.pushButton_10, SIGNAL("clicked()"), self.pokSpe )
        self.connect(self.pushButton_usunj_1, SIGNAL("clicked()"), self.usuSpe)
    #########szefa
        self.connect(self.pushButton_12, SIGNAL("clicked()"), self.pokSze )
        self.connect(self.pushButton_usun_2, SIGNAL("clicked()"), self.usuSze)
    #########admina

        self.connect(self.pushButton_13, SIGNAL("clicked()"), self.pokAdm )
        self.connect(self.pushButton_usun_3, SIGNAL("clicked()"), self.usuSze)
        self.connect(self.pushButtonR_wyslij, SIGNAL("clicked()"), self.wlasne_zapytanie)

    def wlasne_zapytanie(self):
        qw=self.lineEdit.text()
        print qw
        rs=set_querry(qw,colNames=True)
        print rs
        if type(rs) == type([]):
            asd=ShowTable(rs,tabTitle="wynik zapytania")
            asd.show()
            asd.exec_()
        # QMessageBox.information(self, tlumacz(self, "Odpowiedz z bazy") , tlumacz(self,rs))

    def pokAdm(self):

        self.pokPrac(upr=4, pok=True)
        print(self.toolBox_2.currentIndex())
        if (self.toolBox_2.currentIndex() == 3 and self.crPrac):
            self.lineEdit_24.setText(tlumacz(self, self.crPrac[1]))
            self.lineEdit_25.setText(tlumacz(self, self.crPrac[2]))


    def usuSze(self):
        print(self.crPrac)
        if self.crPrac:
            qw=u"DELETE FROM Pracownik_has_Uprawnienia WHERE Pracownik_idPracownik = "+unicode(self.crPrac[0])+";"
            # print(qw)
            set_querry(qw)
            self.crPrac = None


    def pokSze(self):

        self.pokPrac(upr=3, pok=True)
        if (self.toolBox_2.currentIndex() == 2 and self.crPrac):
            self.lineEdit_22.setText(tlumacz(self, self.crPrac[1]))
            self.lineEdit_23.setText(tlumacz(self, self.crPrac[2]))


    def pokPra(self):

        self.pokPrac()

        if (self.toolBox_2.currentIndex() == 0 and self.crPrac):
            self.lineEdit_20.setText(tlumacz(self, self.crPrac[1]))
            self.lineEdit_21.setText(tlumacz(self, self.crPrac[2]))

    def usuPrac(self):
        print(self.crPrac)
        if self.crPrac:
            qw=u"DELETE FROM Pracownik WHERE idPracownik = "+unicode(self.crPrac[0])+";"
            # print(qw)
            set_querry(qw)
            self.crPrac = None

        else:
            return None


    def pokSpe(self):

        self.pokPrac(upr=2, pok=True)

        if (self.toolBox_2.currentIndex() == 1 and self.crPrac):
            self.lineEdit_18.setText(tlumacz(self, self.crPrac[1]))
            self.lineEdit_19.setText(tlumacz(self, self.crPrac[2]))

    def usuSpe(self):
        print("fdgfdgdfgdfgdg")
        if self.crPrac:
            qw=u"DELETE FROM Specjalista WHERE Pracownik_idPracownik = "+unicode(self.crPrac[0])+";"
            print(qw)
            set_querry(qw)
            self.crPrac = None

        else:
            return None


    def dodAdm(self):
        if self.crPrac:
            qw=u" SELECT dodaj_UprawnieniaFUNC("+unicode(self.crPrac[0])+", 4);"
            # print(qw)
            set_querry(qw)
            self.crSzef = None
            self.crPrac = None

        else:
            return None
        # print self.comboBox.currentText()



    def dodSze(self):
        if self.crPrac:
            qw=u" SELECT dodaj_UprawnieniaFUNC("+unicode(self.crPrac[0])+", 3);"
            print qw
            # print(qw)
            set_querry(qw)
            self.crSzef = None
            self.crPrac = None

        else:
            return None
        # print self.comboBox.currentText()


    def dodPrac(self):

        imie=noInject(self.lineEdit_imie.text())
        nazwisko=noInject(self.lineEdit_nazwisko.text())
        email=noInject(self.lineEdit_email.text())
        tel=noInject(self.lineEdit_telefon.text())
        pos=noInject(self.lineEdit_posada.text())
        login=noInject(self.lineEdit_login.text())
        haslo=noInject(self.lineEdit_haslo.text())

        qw="INSERT INTO pracownik VALUES (DEFAULT, '"+imie+"', '"+nazwisko+"', '"+email+"','"+tel+"','"+pos+"','"+login+"','"+haslo+"');"

        set_querry(qw)

    def dodSpec(self):
        if self.crSzef and self.crPrac:
            qw=u"SELECT dodaj_SpecjalisteFUNC("+unicode(self.crPrac[0])+", "+unicode(self.crSzef[0])+",(SELECT idDziedzina FROM dziedzina WHERE opisDziedzina = '"+unicode(self.comboBox.currentText())+"' ));"
            # print(qw)
            set_querry(qw)
            self.crSzef = None
            self.crPrac = None

        else:
            return None
        # print self.comboBox.currentText()

    def pokPracNad(self):
        self.pokPrac(upr=4)
        if (self.toolBox.currentIndex() == 3 and self.crPrac):
            self.lineEdit_10.setText(tlumacz(self, self.crPrac[1]))
            self.lineEdit_11.setText(tlumacz(self, self.crPrac[2]))

    def pokPracNsz(self):
        self.pokPrac(upr=3)
        if (self.toolBox.currentIndex() == 2 and self.crPrac):
            self.lineEdit_6.setText(tlumacz(self, self.crPrac[1]))
            self.lineEdit_7.setText(tlumacz(self, self.crPrac[2]))

    def pokPracNsp(self):
        self.pokPrac(upr=2)
        if (self.toolBox.currentIndex() == 1 and self.crPrac):
            self.lineEdit_2.setText(tlumacz(self, self.crPrac[1]))
            self.lineEdit_3.setText(tlumacz(self, self.crPrac[2]))

    def pokPrac(self, upr=None, pok=False):
        if upr:
            qw="SELECT idPracownik as ID, imie, nazwisko FROM pracownikSC " \
               "WHERE idPracownik"+( " " if pok else " NOT " )+"IN(SELECT Pracownik_idPracownik FROM Pracownik_has_Uprawnienia WHERE" \
               " Uprawnienia_idUprawnienia="+unicode(upr)+" ) ORDER BY ID;"
            # print qw
        else:
            qw="SELECT idPracownik as ID, imie, nazwisko FROM pracownikSC " \
           "ORDER BY ID;"

            print(qw)

        self.crPrac=qerryTable(qw,op=True,colNames=True,title="Dostępni pracownicy")

    def pokSzef(self):


        qw="SELECT idPracownik as ID, imie, nazwisko FROM pracownikSC " \
           "WHERE idPracownik IN(SELECT Pracownik_idPracownik FROM Pracownik_has_Uprawnienia WHERE Uprawnienia_idUprawnienia=3)" \
           "ORDER BY ID;"
        self.crSzef =qerryTable(qw,op=True,colNames=True,title="Szefowie")
        #print(self.crSzef[0])

        if (self.toolBox.currentIndex() == 1 and self.crSzef):
            self.lineEdit_4.setText(tlumacz(self, self.crSzef[1]))
            self.lineEdit_5.setText(tlumacz(self, self.crSzef[2]))



    def retranslateUi2(self):
        # self.comboBox.addItem("")
        # self.comboBox.setItemText(0,tlumacz(self,'dfsjdfs'))

        qw="SELECT opisDziedzina FROM dziedzina;"

        res=set_querry(qw)
        for a in range(len(res)):
            print a
            self.comboBox.addItem("")
            self.comboBox.setItemText(a,tlumacz(self,res[a][0]))

    def wszyscy(self):

        tab='pracownik'
        a = getTable(tab,True)
        form = ShowTable(a,tab)
        form.exec_()
        form.show()

        # if(form.exec_() == 0 ):
            # fgfg = form.getValues();
            # print(fgfg)

    def specjalisci(self):
        qw="SELECT * FROM pracownikSC WHERE idPracownik IN(SELECT Pracownik_idPracownik FROM Specjalista);"
        qerryTable(qw,colNames=True,title="specjaliści")

    def szefowie(self):
        qw="SELECT * FROM pracownikSC WHERE idPracownik IN(SELECT Pracownik_idPracownik FROM Pracownik_has_Uprawnienia WHERE Uprawnienia_idUprawnienia=3);"
        qerryTable(qw,colNames=True,title="Szefowie")

    def admini(self):
        qw="SELECT * FROM pracownikSC WHERE idPracownik IN(SELECT Pracownik_idPracownik FROM Pracownik_has_Uprawnienia WHERE Uprawnienia_idUprawnienia=4);"
        qerryTable(qw,colNames=True,title="Admini")

    def specjalizacje(self):
        qw="SELECT Dziedzina.opisDziedzina AS dostepne_specjalnosci, COUNT(Dziedzina_has_Specjalista.Specjalista_idSpecjalista) as ludzi FROM" \
           " Dziedzina_has_Specjalista, Dziedzina WHERE Dziedzina.idDziedzina=Dziedzina_has_Specjalista.Dziedzina_idDziedzina " \
           "GROUP BY Dziedzina.opisDziedzina;"
        qerryTable(qw,colNames=True,title="Dostępne specjalizacje")

    def braki(self):

        qw="SELECT DISTINCT Dziedzina.opisDziedzina AS brakujace_specjalnosci FROM Dziedzina" \
           " WHERE idDziedzina NOT IN (SELECT Dziedzina_idDziedzina FROM Dziedzina_has_Specjalista);"
        qerryTable(qw,colNames=True,title="Brakujące specjalizacje")

    def obciazenie(self):
        qw="SELECT Pracownik.imie as Imie, Pracownik.nazwisko AS Nazwisko, Obciazenie_szefow.obciazenie_specjalistow AS Obciazenie  " \
           "FROM  Obciazenie_szefow JOIN Pracownik ON Pracownik.idPracownik=Obciazenie_szefow.szef;"

        qerryTable(qw,colNames=True,title="Szef i obciążenie jego specjalistów")



if __name__ == '__main__':
    app = QApplication(sys.argv)
    a=4
    admin = AdminDialog(4)
    admin.show()
    app.exec_()
