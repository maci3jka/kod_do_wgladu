# -*- coding: utf-8 -*-
__author__ = 'maciek'

### kompilacja okien z QtDesigner
#
#  pyside-uic input_file.ui -o output_file.py
#
###


from PySide.QtGui import *
from PySide.QtCore import *
import sys
# import psycopg2 # komuniacja z baza
import funkcje
# okna
import loginWindow
# import adminMainWindow
import showTableWindow
import dbErrorWindow
from showTable import *


class ErrorDialog(QDialog, dbErrorWindow.Ui_Dialog):
    def __init__(self,err,parent=None,):
        super(ErrorDialog, self).__init__(parent)
        self.setupUi(self)
        self.err=err
        self.textEdit.setText(funkcje.tlumacz(self,self.err))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    a=4
    admin = ErrorDialog("ąćź")

    admin.show()
    app.exec_()
