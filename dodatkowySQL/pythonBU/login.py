# -*- coding: utf-8 -*-
__author__ = 'maciek'

### kompilacja okien z QtDesigner
#
#  pyside-uic input_file.ui -o output_file.py
#
###


from PySide.QtGui import *
from PySide.QtCore import *
# import sys
# import psycopg2 # komuniacja z baza
from funkcje import *
# okna
import loginWindow
# import adminMainWindow
import showTableWindow
from adminDialog import *
from showTable import *
from pracownikDialog import *
from specjalistaDialog import *
class LoginDialog(QDialog, loginWindow.Ui_LOGIN):
    def __init__(self, parent=None):
        super(LoginDialog, self).__init__(parent)
        self.setupUi(self)
        self.lineEditHaslo.setEchoMode(QLineEdit.Password )

        self.connect(self.buttonBox, SIGNAL("accepted()"), self.db_login)
        self.connect(self.buttonBox, SIGNAL("rejected()"), quit)
        # self.connect(self.lineEditUzytkownik,SIGNAL("returnPressed ()"), self.lineEditHaslo.selectAll)
        self.mainW = None

    def db_login(self):
        user = self.lineEditUzytkownik.text()
        passwd = self.lineEditHaslo.text()
        acces = self.comboBox.currentIndex()
        #print acces
        gdzie = ['login', 'haslo']
        co = [user, passwd]
        #print user
        # self.close()
        self.hide()
        #print passwd
        user_log = find_in_db('pracownik', gdzie, co)
        if len(user_log)==0:
            print user
            # QMessageBox.information(self, tlumacz(self, "Nieoprawne logowanie!") , tlumacz(self,"Nie znaleziono logina %s"  % (user)))
            self.show()
            return 0
        userDB = find_in_db('pracownik', 'idpracownik' , user_log[0][0])
        userUP = find_in_db('Pracownik_has_Uprawnienia', 'Pracownik_idPracownik' , user_log[0][0])
        print userUP
        userUP1 = [cn[0] for cn in userUP]
        print userDB[0][-1]
        printstr="Zalogowano\n"+format(userDB[0][1])+"\n"+format(userDB[0][2])
        if ((acces ==0) and (1 in userUP1 )):
            printstr=printstr+"\n jako pracownika"
            QMessageBox.information(self, tlumacz(self, "Poprawne logowanie!") , tlumacz(self,printstr))
            # print "panel użytkownika"
            fdmainW = PracownikDialog(user_log[0][0])
            fdmainW.show()
            if(fdmainW.exec_()==0):
                self.show()

        elif ((acces ==1) and (2 in userUP1 )):
            printstr=printstr+"\n jako specjalistę"
            QMessageBox.information(self, tlumacz(self, "Poprawne logowanie!") , tlumacz(self,printstr))
            # print "panel specjalisty"
            fdmainW = SpecDialog(user_log[0][0])
            fdmainW.show()
            if(fdmainW.exec_()==0):
                self.show()
        elif ((acces ==2) and (3 in userUP1 )):
            printstr=printstr+"\n jako szefa"
            print "panel szefa"
            QMessageBox.information(self, tlumacz(self, "Poprawne logowanie!") , tlumacz(self,printstr+"\n NIE ZAIMPLEMETOWANE"))
        elif ((acces ==3) and (4 in userUP1 )):
            printstr=printstr+"\n jako admina"
            print "panel admina"
            QMessageBox.information(self, tlumacz(self, "Poprawne logowanie!") , tlumacz(self,printstr))
            #print user_log[0][0]
            fdmainW = AdminDialog(user_log[0][0])
            fdmainW.show()
            if(fdmainW.exec_()==0):
                self.show()
        else:
            printstr="Nie można zalogować!"
            QMessageBox.information(self, tlumacz(self, "Poprawne logowanie!") , tlumacz(self,printstr))

            self.show()

        return 0


        # if not self.mainW.isVisible():
        #     self.show()
        #     print "asdfsf"

    def zygni_baze(self):
        con = None
        try:
            # conn_string = "host='localhost' dbname='zad' user='python' password='python'"
            #conn_string = "host='localhost' dbname='test' user='maciek' password='m'"
            con = psycopg2.connect(conn_string)

            cur = con.cursor()
            #cur.execute("SELECT dodaj_pracownika(2, 'Arek', 'Zając', 'az@email.pl','7000855468','stojak','az','az')")
            #cur.execute("SELECT dodaj_pracownika(2, 'Arek', 'Zając', 'az@email.pl','7000855468','stojak','az','az')")
            ex = 'SELECT * FROM %s ;' % 'uzytkownik'

            cur.execute(ex)

            col_names = [cn[0] for cn in cur.description]

            rows = cur.fetchall()

            print col_names
            print rows
            print cur.description
            for row in rows:
                print row




        except psycopg2.DatabaseError, e:
            print 'Error %s' % e
            """
            Show the information message
            """
            QMessageBox.information(self, tlumacz(self, "Błąd!"), tlumacz(self, "{0:s}".format(e)))
            # QtGui.QApplication.translate("LOGIN", "Hasło", None, QtGui.QApplication.UnicodeUTF8)
            sys.exit(1)


        finally:

            if con:
                con.close()


def startGui():
    app = QApplication(sys.argv)
    form = LoginDialog()
    # form = AdminDialog()
    # asd.show()
    # form = GraphsDialog()
    form.show()
    app.exec_()

def testTable():
    tab='pracownik'
    a = getTable(tab,True)
    for b in a:
        print(b)
    app = QApplication(sys.argv)
    form = ShowTable(a,tab)
    form.show()
    app.exec_()


if __name__ == '__main__':
    #testTable()
    startGui()
