# -*- coding: utf-8 -*-

__author__ = 'maciek'


from PySide.QtGui import *
from PySide.QtCore import *
# from showTable import *
import showTable
import sys
import dbError
import psycopg2 # komuniacja z baza


conn_string = "host='localhost' dbname='zad' user='python' password='python'"
user = None

def set_querry(qw, colNames=False):
    con = None
    try:
        con = psycopg2.connect(conn_string)

        cur = con.cursor()
        cur.execute(qw)
        con.commit()

        rowst = list(cur.fetchall())
        rows = [list(c) for c in rowst]
        if colNames:
            col_names = [cn[0] for cn in cur.description]
            rows.insert(0,col_names)
        # print(type(rows))
        return rows
        # con.commit()

    except psycopg2.DatabaseError, e:
        print 'Error %s' % e
        # print 'rakieta'
        # reply = QMessageBox(QWidget,"sdfsdfsdfsdfsdf")
        """
            Show the information message
            """
        # print type(str(e))
        er=str(e)
        if 'no results to fetch' in er:
            er="dane przyjęto"
        asd=dbError.ErrorDialog(er)
        asd.show()
        asd.exec_()
        # return e
        # DO ZROBIENIA
        # QMessageBox.information(self, tlumacz(self, "Błąd!") , tlumacz(self,"{0:s}".format(e)))
        #QtGui.QApplication.translate("LOGIN", "Hasło", None, QtGui.QApplication.UnicodeUTF8)
        #sys.exit(1)


    finally:

        if con:
            con.close()


def find_in_db(table, where, what, colNames=False):
    con = None
    if(type(what) == type([])):
            czego = str(where[0]) + "='" + str(what[0]) + "' "
            for a in range(1, len(where)):
                czego = czego + "AND " + str(where[a]) + "='" + str(what[a]) + "' "
    else:
        czego = str(where) + "='" + str(what) + "' "


    ex = 'SELECT * FROM %s WHERE %s;' % ( table, czego)
    resp = set_querry(ex, colNames)
    return  resp

def getTable(table,colNames=False):
    ex = 'SELECT * FROM %s ;' % ( table)
    resp = set_querry(ex, colNames)
    return  resp

def qerryTable(qw,op=False,colNames=False,title=""):

    # qw="SELECT * FROM pracownikSC WHERE idPracownik IN(SELECT Pracownik_idPracownik FROM Specjalista);"
    a=set_querry(qw, colNames)
    # print(qw)
    if op:
        form= showTable.ShowTableOption(a,title)
        form.show()
        if(form.exec_() == 0 ):
            return  form.getValues();
    else:
        form = showTable.ShowTable(a,title)
        form.exec_()

def tlumacz(obj, text):
    return QApplication.translate(obj.objectName(), text, None, QApplication.UnicodeUTF8)

def noInject(qw):
    if((";" in qw) or ("=" in qw)) or ("'" in qw):
        print "wykryto nielegalny znak"
        return None
    else:
        return qw
# def blad()
if __name__ == '__main__':
    a = set_querry(qw="select current_database()",colNames=True)
    print "odpowiedz:"
    print a