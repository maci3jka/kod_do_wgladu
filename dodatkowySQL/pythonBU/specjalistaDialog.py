# -*- coding: utf-8 -*-
__author__ = 'maciek'

from PySide.QtGui import *
from PySide.QtCore import *
from showTable import *
from funkcje import *

import specjalistaMainWindow
from dbError import *

from funkcje import *
class SpecDialog(QDialog, specjalistaMainWindow.Ui_Dialog):
    def __init__(self,currID, parent=None):
        super(SpecDialog, self).__init__(parent)
        self.setupUi(self)
        self.currID=currID
        self.specID=self.getSpecId()

        self.connect(self.pushButton_zamknij, SIGNAL("clicked()"), quit)
        self.connect(self.pushButton_wyloguj, SIGNAL("clicked()"), self.close)
        self.connect(self.pushButton_6, SIGNAL("clicked()"), self.showspec)
        self.connect(self.pushButton, SIGNAL("clicked()"), self.dodajw)
        self.connect(self.pushButton_2, SIGNAL("clicked()"), self.usunw)
        self.connect(self.pushButton_5, SIGNAL("clicked()"), self.wykonaj)
        self.connect(self.pushButton_11, SIGNAL("clicked()"), self.szefy)
        self.stat=None

    def dodajw(self):
        self.showspec(dodaj=True)
        self.stat=1

    def usunw(self):
        self.showspec(usun=True)
        self.stat=2

    def showspec(self,dodaj=False, usun=False ):
        if dodaj:

            qw="SELECT Dziedzina.opisdziedzina AS Specjalnosc FROM" \
               " Dziedzina " \
               "WHERE Dziedzina.idDziedzina NOT IN (SELECT Dziedzina_idDziedzina FROM Dziedzina_has_Specjalista WHERE specjalista_idspecjalista="+unicode(self.specID)+") " \
               ";"
            rs=qerryTable(qw,op=True,colNames=True,title="Dostępne dziedziny")
            self.lineEdit.setText(tlumacz(self, rs[0]))

        else:
            # qw="SELECT Dziedzina.opisdziedzina AS Specjalnosc FROM Dziedzina_has_Specjalista JOIN Dziedzina ON Dziedzina.idDziedzina=Dziedzina_has_Specjalista.Dziedzina_idDziedzina WHERE" \
            #    " Dziedzina_has_Specjalista.Specjalista_idSpecjalista="+unicode(self.specID)+" ;"
            qw="SELECT Dziedzina.opisdziedzina AS Specjalnosc FROM" \
               " Dziedzina " \
               "WHERE Dziedzina.idDziedzina IN (SELECT Dziedzina_idDziedzina FROM Dziedzina_has_Specjalista WHERE specjalista_idspecjalista="+unicode(self.specID)+") " \
               ";"
            #sprawdzic
            # rs=qerryTable(qw,op=True,colNames=True,title="Posiadane dziedziny")
            rs=qerryTable(qw,op=True,colNames=True,title="Dostępne dziedziny")
            print rs
            if usun:
               self.lineEdit.setText(tlumacz(self,rs[0]))
        # self.crSzef =qerryTable(qw,op=True,colNames=True,title="Szefowie")

    def wykonaj(self):
        if self.stat:
            print self.stat
            if self.stat == 1 :

                qw="INSERT INTO  Dziedzina_has_Specjalista VALUES ((Select idDziedzina FROM Dziedzina WHERE opisdziedzina='"+unicode(self.lineEdit.text())+"'), "+unicode(self.specID)+" )  ;"


            elif self.stat == 2 :
                qw="DELETE FROM  Dziedzina_has_Specjalista WHERE dziedzina_iddziedzina = (Select idDziedzina FROM Dziedzina WHERE opisdziedzina='"+unicode(self.lineEdit.text())+"') AND specjalista_idspecjalista="+unicode(self.specID)+"   ;"
                print qw
            re=set_querry(qw)


    def getSpecId(self):
        qw="SELECT idSpecjalista FROM Specjalista, Pracownik " \
           "WHERE Specjalista.Pracownik_idPracownik="+unicode(self.currID)+" AND specjalista.Pracownik_idPracownik=pracownik.idpracownik ;"
        re=set_querry(qw)
        print re[0][0]
        return re[0][0]

    def szefy(self):
        qw="SELECT Pracownik.imie, Pracownik.nazwisko FROM" \
               " Pracownik, Specjalista " \
               "WHERE Pracownik.idPracownik = Specjalista.szef_idpracownik AND Specjalista.idSpecjalista = "+unicode(self.specID)+" " \
               ";"
        rs=qerryTable(qw,op=False,colNames=True,title="Dostępne dziedziny")













if __name__ == '__main__':
    app = QApplication(sys.argv)
    a=4
    s = SpecDialog(1)

    s.show()
    app.exec_()