# -*- coding: utf-8 -*-
__author__ = 'maciek'

from PySide.QtGui import *
from PySide.QtCore import *
import funkcje
import adminMainWindow
import showTableWindow
import datetime


class ShowTable(QDialog, showTableWindow.Ui_Dialog):
    def __init__(self, table, tabTitle ='', parent=None):
        super(ShowTable, self).__init__(parent)
        self.setupUi(self)
        self.tabTitle = tabTitle
        self.title = table[0]
        self.data = table[1:]
        self.retranslateUi()
        self.pushButton.setText(funkcje.tlumacz(self,"Zapisz"))

        self.connect(self.pushButton,  SIGNAL("clicked()"), self.saveTable)

    def retranslateUi(self):

        self.tableWidget.setObjectName(self.tabTitle)
        self.tableWidget.setColumnCount(len(self.title))
        self.tableWidget.setRowCount(len(self.data))
        self.setWindowTitle(funkcje.tlumacz(self, self.tabTitle))
        #tytuly
        for a in range(len(self.title)):
            # print a
            item = QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(a, item)

            self.tableWidget.horizontalHeaderItem(a).setText(funkcje.tlumacz(self, self.title[a]))
            # print funkcje.tlumacz(self, self.title[a])
        #zawartosc
        for a in range(len(self.data)):
            for b in range(len(self.title)):
                item = QTableWidgetItem()
                self.tableWidget.setItem(a, b, item)
                self.tableWidget.item(a, b).setText(funkcje.tlumacz(self, str(self.data[a][b])))

    def saveTable(self):
        fileObj = QFileDialog.getSaveFileName(caption="zapisz table",filter="* .txt (*.txt)")
        # print(fileObj)
        line=str(datetime.datetime.now().time())
        try:
            file = open(fileObj[0], "w")
            file.write(line+"\n")

            line=" | ".join(self.title)
            print("ala")
            file.write(line+"\n")
            print(self.data)
            for a in self.data:

                print a
                c = [str( b) for b in a]
                line=" | ".join(c)
                # line=join(str(v) for v in a)
                file.write(line+"\n")
                print a

            file.close()
        except Exception, e:
            print e
            print("dfsdfsdf")
            quit()
        return None


    def getValues(self):
        return 142
#
class ShowTableOption(QDialog, showTableWindow.Ui_Dialog):
    def __init__(self, table, tabTitle ='', parent=None):
        super(ShowTableOption, self).__init__(parent)
        self.setupUi(self)
        self.tabTitle = tabTitle
        self.title = table[0]
        self.data = table[1:]
        self.retranslateUi()
        self.connect(self.pushButton,  SIGNAL("clicked()"), self.wybor)
        self.pushButton.setText(funkcje.tlumacz(self,"Wybrałem"))
        self.retVal=None

    def retranslateUi(self):

        self.tableWidget.setObjectName(self.tabTitle)
        self.tableWidget.setColumnCount(len(self.title))
        self.tableWidget.setRowCount(len(self.data))
        self.setWindowTitle(funkcje.tlumacz(self, self.tabTitle))
        self.pushButton.setText(funkcje.tlumacz(self,"Wybrałem"))
        #tytuly
        for a in range(len(self.title)):
            # print a
            item = QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(a, item)
            self.tableWidget.horizontalHeaderItem(a).setText(funkcje.tlumacz(self, self.title[a]))
            # print funkcje.tlumacz(self, self.title[a])
        #zawartosc
        for a in range(len(self.data)):
            for b in range(len(self.title)):
                item = QTableWidgetItem()
                self.tableWidget.setItem(a, b, item)
                self.tableWidget.item(a, b).setText(funkcje.tlumacz(self, str(self.data[a][b])))



        # print("Saving Parameters to Xml")
        # print(fileObj[0])
        # xmlHandle.to_xml_2(par,fileObj[0][:-4])
        # print("Saved Parameters to Xml")

        # return None
    def wybor(self):

        self.retVal = self.tableWidget.currentRow()
        self.close()
    def getValues(self):
        if not self.retVal:
            return None
        return self.data[self.retVal]

# if __name__ == '__main__':
#     app = QApplication(sys.argv)
#     a=4
#     admin = AdminDialog(4)
#     admin.show()
#     app.exec_()
