# -*- coding: utf-8 -*-
__author__ = 'maciek'


from PySide.QtGui import *
from PySide.QtCore import *
from showTable import *
from funkcje import *
import pracownikMainWindow
import adminMainWindow
from dbError import *

from funkcje import *



class PracownikDialog(QDialog, pracownikMainWindow.Ui_Dialog):
    def __init__(self, currentId, parent=None):
        super(PracownikDialog, self).__init__(parent)
        self.setupUi(self)
        self.currentId=currentId
        self.connect(self.pushButton_zamknij, SIGNAL("clicked()"), quit)
        self.connect(self.pushButton_wyloguj, SIGNAL("clicked()"), self.close)
        self.connect(self.pushButton, SIGNAL("clicked()"), self.aktualne)
        self.connect(self.pushButton_2, SIGNAL("clicked()"), self.zakonczone)
        self.connect(self.commandLinkButton, SIGNAL("clicked()"), self.dodajZgloszenie)
        self.connect(self.lineEdit_haslo_2, SIGNAL("editingFinished()"), self.hasla)
        self.connect(self.pushButton_Dodaj_prac, SIGNAL("clicked()"), self.uaktualnijOs)
        self.connect(self.pushButton_Dodaj_prac_2, SIGNAL("clicked()"), self.daneOs)
        self.setDziedzina()


    def dodajZgloszenie(self):
        opis = noInject(self.plainTextEdit.toPlainText())
        dziedzina =self.comboBox.currentText()
        # print dziedzina
        qw=" SELECT dodaj_Zgloszenie("+unicode(self.currentId)+", NULL, '"+unicode(opis)+"',(SELECT idDziedzina FROM dziedzina WHERE opisDziedzina = '"+unicode(dziedzina)+"'));"
        # qw="SELECT idDziedzina FROM dziedzina WHERE opisDziedzina = '"+unicode(dziedzina)+"' ;"
        set_querry(qw)
        # print re


    def setDziedzina(self):

        qw="SELECT opisDziedzina FROM dziedzina;"

        res=set_querry(qw)
        for a in range(len(res)):
            # print a
            self.comboBox.addItem("")
            self.comboBox.setItemText(a,tlumacz(self,res[a][0]))

    def aktualne(self):

        qw ="SELECT nr, Pracownik.imie as oddelegowany, Pracownik.nazwisko as szef, stan , opisDziedzina as dziedzna, data_zgloszenia, opis  FROM" \
            " Zgloszenie_full, Pracownik WHERE pracownik = "+unicode(self.currentId)+" AND data_zamkniecia IS NULL " \
            "AND Pracownik.idPracownik = Zgloszenie_full.szef " \
            "ORDER BY data_zgloszenia ;"

        # qw="SELECT * FROM pracownikSC WHERE idPracownik IN(SELECT Pracownik_idPracownik FROM Pracownik_has_Uprawnienia WHERE Uprawnienia_idUprawnienia=4);"
        qerryTable(qw,colNames=True,title="Trwające zgłoszenia")

    def zakonczone(self):

        qw ="SELECT nr, Pracownik.imie as oddelegowany, Pracownik.nazwisko as szef, stan , opisDziedzina as dziedzna, data_zgloszenia, opis  FROM" \
        " Zgloszenie_full, Pracownik WHERE pracownik = "+unicode(self.currentId)+" AND data_zamkniecia IS NOT NULL " \
        "AND Pracownik.idPracownik = Zgloszenie_full.szef " \
        "ORDER BY data_zgloszenia ;"

        # qw="SELECT * FROM pracownikSC WHERE idPracownik IN(SELECT Pracownik_idPracownik FROM Pracownik_has_Uprawnienia WHERE Uprawnienia_idUprawnienia=4);"
        qerryTable(qw,colNames=True,title="Trwające zgłoszenia")

    def daneOs(self):
        qw="SELECT * FROM pracownikSC WHERE idPracownik="+unicode(self.currentId)+" ;"
        qerryTable(qw,colNames=True,title="Trwające zgłoszenia")
    def uaktualnijOs(self):
        emali=self.lineEdit_email.text()
        tel=self.lineEdit_telefon.text()
        haslo=self.lineEdit_haslo.text()
        # print len(emali)
        if emali!="":
            qw="Update Pracownik SET email='"+unicode(emali)+"' WHERE idPracownik="+unicode(self.currentId)+";"
            set_querry(qw)
        if tel!="":
            qw="Update Pracownik SET telefon='"+unicode(tel)+"' WHERE idPracownik="+unicode(self.currentId)+";"
            set_querry(qw)
        if haslo!="":
            if(self.hasla()):
                qw="Update Pracownik SET haslo='"+unicode(haslo)+"' WHERE idPracownik="+unicode(self.currentId)+";"
                set_querry(qw)



    def hasla(self):
        haslo=self.lineEdit_haslo.text()
        haslo2=self.lineEdit_haslo_2.text()
        if haslo!=haslo2:
            QMessageBox.warning(self, tlumacz(self, "HASŁO!") , tlumacz(self, "Podane hasła są różne"))
            return False
        else:
            return  True

if __name__ == '__main__':
    app = QApplication(sys.argv)
    a=4
    admin = PracownikDialog(1)

    admin.show()
    app.exec_()