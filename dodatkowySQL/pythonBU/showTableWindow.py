# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'showTableWindow.ui'
#
# Created: Sun Jan 18 21:12:15 2015
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(471, 445)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        self.tableWidget = QtGui.QTableWidget(Dialog)
        self.tableWidget.setEnabled(True)
        self.tableWidget.setGeometry(QtCore.QRect(20, 10, 431, 391))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableWidget.sizePolicy().hasHeightForWidth())
        self.tableWidget.setSizePolicy(sizePolicy)
        self.tableWidget.setRowCount(4)
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setRowCount(4)
        # item = QtGui.QTableWidgetItem()
        # self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setItem(0, 0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setItem(0, 1, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setItem(0, 2, item)
        self.pushButton = QtGui.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(350, 410, 99, 23))
        self.pushButton.setObjectName("pushButton")

        # self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    # def retranslateUi(self, Dialog):
    #     Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
    #     self.tableWidget.verticalHeaderItem(0).setText(QtGui.QApplication.translate("Dialog", "w1", None, QtGui.QApplication.UnicodeUTF8))
    #     self.tableWidget.horizontalHeaderItem(0).setText(QtGui.QApplication.translate("Dialog", "K1", None, QtGui.QApplication.UnicodeUTF8))
    #     self.tableWidget.horizontalHeaderItem(1).setText(QtGui.QApplication.translate("Dialog", "k2", None, QtGui.QApplication.UnicodeUTF8))
    #     __sortingEnabled = self.tableWidget.isSortingEnabled()
    #     self.tableWidget.setSortingEnabled(False)
    #     self.tableWidget.item(0, 0).setText(QtGui.QApplication.translate("Dialog", "el 1", None, QtGui.QApplication.UnicodeUTF8))
    #     self.tableWidget.item(0, 1).setText(QtGui.QApplication.translate("Dialog", "el 2", None, QtGui.QApplication.UnicodeUTF8))
    #     self.tableWidget.item(0, 2).setText(QtGui.QApplication.translate("Dialog", "el 3", None, QtGui.QApplication.UnicodeUTF8))
    #     self.tableWidget.setSortingEnabled(__sortingEnabled)
    #     self.pushButton.setText(QtGui.QApplication.translate("Dialog", "Zapisz", None, QtGui.QApplication.UnicodeUTF8))

