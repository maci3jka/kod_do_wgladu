# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class EditNewsByList(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_edit_news_by_list(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_css_selector("button.btn.btn-default").click()
        driver.find_element_by_name("username").clear()
        driver.find_element_by_name("username").send_keys("maciek")
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys("maciek")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_xpath("(//button[@type='button'])[3]").click()
        # old
        new_txt = "new_txt"
        new_title = "new_title"
        orginal_txt = driver.find_element_by_id("id_text").text
        orginal_title = driver.find_element_by_id("id_title").get_attribute("value")
        print(orginal_txt)
        driver.find_element_by_id("id_text").clear()
        driver.find_element_by_id("id_text").send_keys(new_txt)
        driver.find_element_by_id("id_title").clear()
        driver.find_element_by_id("id_title").send_keys(new_title)
        driver.find_element_by_xpath("//button[@onclick='addForm()']").click()
        self.assertEqual(new_title, driver.find_element_by_css_selector("div.news_title").text)
        self.assertEqual(new_txt, driver.find_element_by_css_selector("p.lead").text)
        # # rollback
        driver.find_element_by_id("btn_1").click()
        # input('3)[enter]')
        driver.find_element_by_id("zmien_btn").click()
        # driver.find_element_by_css_selector("div.col-md-7. > a > button.btn.btn-default").click()
        driver.find_element_by_id("id_text").clear()
        driver.find_element_by_id("id_text").send_keys(orginal_txt)
        driver.find_element_by_id("id_title").clear()
        driver.find_element_by_id("id_title").send_keys(orginal_title)
        driver.find_element_by_xpath("//button[@onclick='addForm()']").click()
        print(orginal_title)
        self.assertEqual(orginal_title, driver.find_element_by_css_selector("div.news_title").text)
        self.assertEqual(orginal_txt, driver.find_element_by_css_selector("p.lead").text)
        driver.find_element_by_css_selector("button.btn.btn-default").click()
        driver.get(self.base_url + "/")
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
