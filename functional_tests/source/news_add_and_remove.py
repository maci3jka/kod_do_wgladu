# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class NewsAddAndRemove(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_news_add_and_remove(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_css_selector("button.btn.btn-default").click()
        driver.find_element_by_name("username").clear()
        driver.find_element_by_name("username").send_keys("maciek")
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys("maciek")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.XPATH, "(//button[@type='submit'])[2]"))
        self.assertEqual("Newsy", driver.find_element_by_xpath("(//button[@type='submit'])[2]").text)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_css_selector("div.col-xs-12").click()
        txt = "sdsfadfaads"
        title = "sdf"
        driver.find_element_by_id("id_text").clear()
        driver.find_element_by_id("id_text").send_keys(txt)
        driver.find_element_by_id("id_title").clear()
        driver.find_element_by_id("id_title").send_keys(title)
        driver.find_element_by_id("id_image").clear()
        driver.find_element_by_id("id_image").send_keys("/home/maciek/Obrazy/b79b3bfae0685fbe4e3eebb6fc43569b.jpg")
        driver.find_element_by_xpath("//button[@onclick='addForm()']").click()
        self.assertEqual(title, driver.find_element_by_css_selector("h2.featurette-heading").text)
        self.assertEqual(txt, driver.find_element_by_css_selector("p.lead").text)
        self.assertTrue(self.is_element_present(By.XPATH, "//img[@alt='Generic placeholder image']"))
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_name("delete_box").click()
        driver.find_element_by_css_selector("button.btn.btn-danger").click()
        self.assertRegexpMatches(self.close_alert_and_get_its_text(), r"^Na pewno usunąć[\s\S]$")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_css_selector("button.btn.btn-default").click()
        driver.get(self.base_url + "/")
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
