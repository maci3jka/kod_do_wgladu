# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class NavbarCheck(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_navbar_check(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        self.assertEqual("Wielomed | Główna", driver.title)
        self.assertTrue(self.is_element_present(By.XPATH, "//img"))
        self.assertTrue(self.is_element_present(By.XPATH, "//li/a"))
        driver.find_element_by_xpath("//li[2]/a").click()
        self.assertTrue(self.is_element_present(By.XPATH, "//li[2]/a"))
        self.assertEqual("O nas", driver.find_element_by_xpath("//li[2]/a").text)
        driver.find_element_by_xpath("//li[3]/a").click()
        self.assertTrue(self.is_element_present(By.XPATH, "//li[3]/a"))
        self.assertEqual("Kontakt", driver.find_element_by_xpath("//li[3]/a").text)
        self.assertIn(driver.find_element_by_xpath("//div[2]/div/div/div").text,
                      "Przychodnia Wielogłowy Niepubliczny Zakład Opieki Zdrowotnej WIELOMED")
        self.assertEqual("Przychodnia Porąbka Iwkowska", driver.find_element_by_xpath("//div[2]/div/div/h2").text)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "button.btn.btn-default"))
        driver.find_element_by_link_text(u"Główna").click()
        self.assertTrue(self.is_element_present(By.LINK_TEXT, u"Główna"))
        self.assertEqual("Główna", driver.find_element_by_link_text(u"Główna").text)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
