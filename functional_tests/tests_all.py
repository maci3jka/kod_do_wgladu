# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re


# wymagana jest baza danycych z 1 newsem oraz uzytkownik 'maciek' o hasle 'maciek'
# jest mozliwe sprawdzenie wrzucenia zdjecia więcej patrz funkcja UserAuthorized.test_news_add_and_remove()
class UserUnauthorized(unittest.TestCase):
    """
    Test niezarejestrowanego uzytkownika
    """

    @classmethod
    def setUpClass(self):
        """
        wloczenie firefoxa i zaladdowanie strony
        :return:
        """
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000"
        self.verificationErrors = []
        self.accept_next_alert = True

    def setUp(self):
        """
        kazdy test zaczyna sie do strony startowej
        :return:
        """
        self.driver.get(self.base_url + "/")

    def test_navbar_check(self):
        """
        sprawdzanie elementow paska zakladek paska zakladek
        :return:
        """
        driver = self.driver
        # driver.get(self.base_url + "/")
        self.assertEqual("Wielomed | Główna", driver.title)
        self.assertTrue(self.is_element_present(By.XPATH, "//img"))
        self.assertTrue(self.is_element_present(By.XPATH, "//li/a"))
        driver.find_element_by_xpath("//li[2]/a").click()
        self.assertTrue(self.is_element_present(By.XPATH, "//li[2]/a"))
        self.assertEqual("O nas", driver.find_element_by_xpath("//li[2]/a").text)
        driver.find_element_by_xpath("//li[3]/a").click()
        self.assertTrue(self.is_element_present(By.XPATH, "//li[3]/a"))
        self.assertEqual("Kontakt", driver.find_element_by_xpath("//li[3]/a").text)
        self.assertIn("Przychodnia Wielogłowy",driver.find_element_by_id("address_Wieloglowy").text)
        self.assertIn("Przychodnia Porąbka Iwkowska", driver.find_element_by_id("address_Iwkowa").text)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "button.btn.btn-default"))
        driver.find_element_by_link_text(u"Główna").click()
        self.assertTrue(self.is_element_present(By.LINK_TEXT, u"Główna"))
        self.assertEqual("Główna", driver.find_element_by_link_text(u"Główna").text)

    def test_news_show(self):
        """
        sprawdzenie porawnego wyswietlenia newsa
        :return:
        """
        driver = self.driver
        # driver.get(self.base_url + "/")
        title = driver.find_element_by_css_selector("div.news_title").text
        reduced_text = driver.find_element_by_css_selector("p.lead").text
        self.assertTrue(self.is_element_present(By.XPATH, "//img[@alt='Generic placeholder image']"))
        self.assertTrue(self.is_element_present(By.XPATH, "(//button[@type='button'])[2]"))
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        self.assertEqual(title, driver.find_element_by_css_selector("div.news_title").text)
        self.assertEqual(reduced_text, driver.find_element_by_css_selector("p.lead").text[0:49])
        driver.find_element_by_link_text(u"Główna").click()

    def test_404_unauthorized_news_edit(self):
        """
        sprawdzenie wyswietlenia bledu 404 dla niezarejstrowanego uzytkownika probujacego edytowac newsy
        :return:
        """
        driver = self.driver
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        current_id = "1"
        driver.get(self.base_url + "/newsEdit/" + current_id + "/")
        self.assertEqual("Page not found at /newsEdit/" + current_id + "/", driver.title)
        driver.get(self.base_url + "/")

    def test_login(self):
        """
        proba zalogowania sie uzytkownika
        w bazie jest uzytkownik maciek o haslie maciek
        :return:
        """
        usr="maciek"
        pwd="maciek"
        driver = self.driver
        # driver.get(self.base_url + "/")
        driver.find_element_by_css_selector("button.btn.btn-default").click()
        driver.find_element_by_name("username").clear()
        driver.find_element_by_name("username").send_keys(usr)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(pwd)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_css_selector("button.btn.btn-default").click()


    def tearDown(self):
        self.assertEqual([], self.verificationErrors)

    @classmethod
    def tearDownClass(self):
        """
        wylaczenie przegladarki
        :return:
        """
        self.driver.quit()

# pomocnicze funkje z selenium ide
    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            print(e)
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            print(e)
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True


class UserAuthorized(unittest.TestCase):
    """
    Test zarejestrowanego uzytkownika
    """

    @classmethod
    def setUpClass(self):
        """
        wloczenie firefoxa i zaladdowanie strony
        oraz logowanie uzytkownika maciek o hasle maciek
        :return:
        """
        usr="maciek"
        pwd="maciek"
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000"
        self.verificationErrors = []
        self.accept_next_alert = True
        self.driver.get(self.base_url + "/")
        self.driver.find_element_by_css_selector("button.btn.btn-default").click()
        self.driver.find_element_by_name("username").clear()
        self.driver.find_element_by_name("username").send_keys(usr)
        self.driver.find_element_by_name("password").clear()
        self.driver.find_element_by_name("password").send_keys(pwd)
        self.driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()

    def setUp(self):
        """
        kazdy test zaczyna sie do strony startowej
        :return:
        """
        self.driver.get(self.base_url + "/")

    def test_news_add_and_remove(self):
        """
        dowawanie i usuwanie newsa
        razem aby baza po tescie zostala nienaruszona
        img: obrazek do zaladowania
        :return:
        """
        img="/home/maciek/Obrazy/b79b3bfae0685fbe4e3eebb6fc43569b.jpg"
        driver = self.driver
        self.assertTrue(self.is_element_present(By.XPATH, "(//button[@type='submit'])[2]"))
        self.assertEqual("Newsy", driver.find_element_by_xpath("(//button[@type='submit'])[2]").text)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_css_selector("div.col-xs-12").click()
        txt = "sdsfadfaads"
        title = "sdf"
        driver.find_element_by_id("id_text").clear()
        driver.find_element_by_id("id_text").send_keys(txt)
        driver.find_element_by_id("id_title").clear()
        driver.find_element_by_id("id_title").send_keys(title)
        driver.find_element_by_id("id_image").clear()
        if img:
            driver.find_element_by_id("id_image").send_keys(img)
        driver.find_element_by_xpath("//button[@onclick='addForm()']").click()
        self.assertEqual(title, driver.find_element_by_css_selector("h2.featurette-heading").text)
        self.assertEqual(txt, driver.find_element_by_css_selector("p.lead").text)
        self.assertTrue(self.is_element_present(By.XPATH, "//img[@alt='Generic placeholder image']"))
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_name("delete_box").click()
        driver.find_element_by_css_selector("button.btn.btn-danger").click()
        self.assertRegexpMatches(self.close_alert_and_get_its_text(), r"^Na pewno usunąć[\s\S]$")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()

    def test_edit_news_by_site(self):
        """
        edycja newsa przez klikniecie |zmien| na stornie
        sprawdza czy sa wlasciwe elementy oraz edytuje
        efektywnie edycja zachodzi dwa razy aby pierwotny stan zostal zachowany
        :return:
        """
        driver = self.driver
        self.assertEqual(u"Więcej", driver.find_element_by_class_name("news_more_btn").text)
        driver.find_element_by_class_name("news_more_btn").click() # czasem potrafi nie kliknac trzaba manualnie
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        self.assertTrue(self.is_element_present(By.ID, "zmien_btn"))
        self.assertEqual(u"Zmień", driver.find_element_by_id("zmien_btn").text)
        driver.find_element_by_id("zmien_btn").click()
        new_txt = "new_txt"
        new_title = "new_title"
        orginal_txt = driver.find_element_by_id("id_text").text
        orginal_title = driver.find_element_by_id("id_title").get_attribute("value")
        driver.find_element_by_id("id_text").clear()
        driver.find_element_by_id("id_text").send_keys(new_txt)
        driver.find_element_by_id("id_title").clear()
        driver.find_element_by_id("id_title").send_keys(new_title)
        driver.find_element_by_xpath("//button[@onclick='addForm()']").click()
        # input("1.[enter]")
        self.assertEqual(new_title, driver.find_element_by_css_selector("div.news_title").text)
        self.assertEqual(new_txt, driver.find_element_by_css_selector("p.lead").text)
        # rollback powrot do wartosci pierwotnej
        driver.find_element_by_class_name("news_more_btn").click() # czasem potrafi nie kliknac trzaba manualnie
        print(driver.find_element_by_id("zmien_btn"))
        driver.find_element_by_id("zmien_btn").click()
        driver.find_element_by_id("id_text").clear()
        driver.find_element_by_id("id_text").send_keys(orginal_txt)
        driver.find_element_by_id("id_title").clear()
        driver.find_element_by_id("id_title").send_keys(orginal_title)
        driver.find_element_by_xpath("//button[@onclick='addForm()']").click()
        # print(orginal_title)
        self.assertEqual(orginal_title, driver.find_element_by_css_selector("div.news_title").text)
        self.assertEqual(orginal_txt, driver.find_element_by_css_selector("p.lead").text)

    def test_edit_news_by_list(self):
        """
        edycja newsa przez liste w panelu edycji postow
        sprawdza czy sa wlasciwe elementy oraz edytuje
        efektywnie edycja zachodzi dwa razy aby pierwotny stan zostal zachowany
        :return:
        """
        driver = self.driver
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_xpath("(//button[@type='button'])[3]").click()
        # old
        new_txt = "new_txt"
        new_title = "new_title"
        orginal_txt = driver.find_element_by_id("id_text").text
        orginal_title = driver.find_element_by_id("id_title").get_attribute("value")
        print(orginal_txt)
        driver.find_element_by_id("id_text").clear()
        driver.find_element_by_id("id_text").send_keys(new_txt)
        driver.find_element_by_id("id_title").clear()
        driver.find_element_by_id("id_title").send_keys(new_title)
        driver.find_element_by_xpath("//button[@onclick='addForm()']").click()
        self.assertEqual(new_title, driver.find_element_by_css_selector("div.news_title").text)
        self.assertEqual(new_txt, driver.find_element_by_css_selector("p.lead").text)
        # # rollback

        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_xpath("(//button[@type='button'])[3]").click()
        # driver.find_element_by_css_selector("div.col-md-7. > a > button.btn.btn-default").click()
        driver.find_element_by_id("id_text").clear()
        driver.find_element_by_id("id_text").send_keys(orginal_txt)
        driver.find_element_by_id("id_title").clear()
        driver.find_element_by_id("id_title").send_keys(orginal_title)
        driver.find_element_by_xpath("//button[@onclick='addForm()']").click()
        # print(orginal_title)
        self.assertEqual(orginal_title, driver.find_element_by_css_selector("div.news_title").text)
        self.assertEqual(orginal_txt, driver.find_element_by_css_selector("p.lead").text)

    def tearDown(self):
        self.assertEqual([], self.verificationErrors)

    @classmethod
    def tearDownClass(self):
        """
        wylogowanie i wylaczenie przegladarki
        :return:
        """
        self.driver.find_element_by_css_selector("button.btn.btn-default").click()
        self.driver.get(self.base_url + "/")
        self.driver.quit()

# pomocnicze funkje z selenium ide
    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            print(e)
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            print(e)
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True


if __name__ == "__main__":
    unittest.main()
