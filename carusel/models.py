# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from news.models import News

class Carusel(models.Model):

    title = models.CharField(verbose_name=_("Tytuł"), max_length=1000)
    subtitle = models.CharField(verbose_name=_("Podtytuł"), max_length=1000)

    link_CHOICES = (('U', 'URL'), ('N', 'News'), ('B', 'Brak'))
    link = models.CharField(max_length=1, choices=link_CHOICES, unique=False, null=False, default='B')
    url = models.URLField(verbose_name=_("Link Zewnętrzy"), null=True, blank=True)
    news = models.ForeignKey(News, null=True, blank=True)
    image = models.ImageField(verbose_name=_("Obrazek"), upload_to='slajd', blank=False)

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Slajd"
        verbose_name_plural = "Slajdy"
