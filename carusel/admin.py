from django.contrib import admin
from .models import Carusel

class CaruselAdmin(admin.ModelAdmin):

    model = Carusel
    search_fields = ('title','subtitle')
    list_display = ('title', 'subtitle', 'link')

admin.site.register(Carusel, CaruselAdmin)