from django.contrib import admin
from .models import Diag, DiagGroup
# Register your models here.
class DiagAdmin(admin.ModelAdmin):
    """
    dodawanie lekarzy
    """
    # formfield_overrides = {
    #     models.ImageField: {
    #         'widget': ImageThumbnailFileInput()
    #     },
    # }
    model = Diag
    search_fields = ('title','icd','symbol')

    list_display = ('title','icd','symbol')
    # add_fieldsets = (
    #     (None, {
    #         'fields': ('title',
    #                    'name',
    #                    'surname',
    #                    'extras',
    #                    ),
    #     }),
    # )
# dolaczenie do panelu admina modelu News
admin.site.register(Diag,DiagAdmin)

class DiagGroupAdmin(admin.ModelAdmin):
    """
    dodawanie lekarzy
    """
    # formfield_overrides = {
    #     models.ImageField: {
    #         'widget': ImageThumbnailFileInput()
    #     },
    # }
    model = DiagGroup
    # list_display = ('name','surname')
    # add_fieldsets = (
    #     (None, {
    #         'fields': ('title',
    #                    'name',
    #                    'surname',
    #                    'extras',
    #                    ),
    #     }),
    # )
# dolaczenie do panelu admina modelu News
admin.site.register(DiagGroup,DiagGroupAdmin)