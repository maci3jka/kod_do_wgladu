from django import forms
from .models import Diag


class DiagForm(forms.Form):
    title = forms.CharField(label='Nazwa badania', max_length=100)

    class Meta:
        model = Diag
        fields = ('title')
