# -*- coding: utf-8 -*-
# Create your views here.
from django.shortcuts import render, get_object_or_404, redirect, Http404
from django.template import RequestContext
from .forms import DiagForm
from .models import Diag, DiagGroup
from itertools import chain
# result_list = list(chain(page_list, article_list, post_list))
def diag_list(request):

    template = "diag_lists.html"
    # doctors_datest = []
    diag_groups = DiagGroup.objects.order_by('id')

    diag_list = [{'diag_group': diag_group,'diag':Diag.objects.filter(diag_group=diag_group).order_by('id') }for diag_group in diag_groups]
    # for diag_group in diag_groups:
    #     dates_I = DiagGroup.objects.filter(diag_group=diag_group,  przychodnia="I")
    #     if len(dates_W) > 0:
    #         diag_list.append({'diag_group': diag_group, 'dates': dates_W})
    context = {
        'diag_list':diag_list,
    }
    # for doctors in doctors_datest:
    #     print(doctors['dates'].get_shirt_size_display())
    return render(request, template,context=context)


def diagnostics_list(request):

    template = "group_lists.html"

        # request.GET
        # title = request.GET['title']
    title = request.GET.get('title')
    if title is '':
        diags3 = None
    else:
        # diags = Diag.objects.filter(title__contains=request.GET['title']).order_by('id')
        diags = Diag.objects.filter(title__startswith=request.GET['title']).order_by('title')
        # print(diags)
        diags2 = Diag.objects.filter(title__contains=request.GET['title']).order_by('title').exclude(title__startswith=request.GET['title']) # + diags
        # print(diags2)
        diags3 = list(chain(diags,diags2))
        # diags3 = diags3.order_by('word')
        # print(diags3)

        if len(diags) == 0:
            diags3 = -1
    # except IndexError as e:
    #     print("Eception: %s" % e)
    #     diags = None
    #     diags3 = None
    #
    # if request.GET['title'] is not None:
    #     diags = Diag.objects.filter(title__contains=request.GET['title']).order_by('id')
    # else:
    #     diags = None
    diag_groups = DiagGroup.objects.order_by('id')
    form = DiagForm(request.GET or None)
    # print("diags %s" % str(diags))
    context = {
        'diags': diags3,
        'diag_groups': diag_groups,
        'form': form,
    }

    return render(request, template,context=context)

def diag_detail(request, diag_group_id):

    template = "diag_lists.html"
    # doctors_datest = []
    diag_groups = DiagGroup.objects.get(id=diag_group_id)

    diag_list = [{'diag_group': diag_groups,'diag':Diag.objects.filter(diag_group=diag_groups).order_by('id') }]
    # for diag_group in diag_groups:
    #     dates_I = DiagGroup.objects.filter(diag_group=diag_group,  przychodnia="I")
    #     if len(dates_W) > 0:
    #         diag_list.append({'diag_group': diag_group, 'dates': dates_W})
    context = {
        'diag_list':diag_list,
    }
    # for doctors in doctors_datest:
    #     print(doctors['dates'].get_shirt_size_display())
    return render(request, template,context=context)
