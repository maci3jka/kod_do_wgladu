# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _


class DiagGroup(models.Model):
    title = models.CharField(verbose_name=_("Nazwa grup badań"), max_length=1000)

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _("Typ Badań")
        verbose_name_plural = _("Typy Badań")

class Diag(models.Model):

    title = models.CharField(verbose_name=_("Nazwa badania"), max_length=1000)
    icd = models.CharField(verbose_name=_("Kod ICD"), max_length=1000, null=True)
    symbol = models.CharField(verbose_name=_("Symbol"), max_length=1000)
    material = models.CharField(verbose_name=_("Materiał"), max_length=1000, null=True)
    wait_time = models.IntegerField(verbose_name=_("Czas oczekiwania"))
    price = models.FloatField(verbose_name=_("Cena"))
    adnotation = models.CharField(verbose_name=_("Uwagi"), max_length=1000, null=True)
    diag_group = models.ForeignKey(DiagGroup)

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _("Badanie")
        verbose_name_plural = _("Badania")
