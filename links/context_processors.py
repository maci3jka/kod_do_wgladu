# -*- coding: utf-8 -*-

# from datetime import datetime
from .models import HeadLink


def add_head_links(request):
    context_data = dict()
    context_data['head_links'] = HeadLink.objects.filter(show=True).order_by('-priority')
    return context_data
