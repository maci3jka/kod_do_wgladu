from django.contrib import admin
from .models import Link, HeadLink
# Register your models here.
class LinkAdmin(admin.ModelAdmin):
    """
    dodawanie lekarzy
    """
    # formfield_overrides = {
    #     models.ImageField: {
    #         'widget': ImageThumbnailFileInput()
    #     },
    # }
    model = Link
    list_display = ('title','priority')
    search_fields = ('title',)
    # add_fieldsets = (
    #     (None, {
    #         'fields': ('title',
    #                    'name',
    #                    'surname',
    #                    'extras',
    #                    ),
    #     }),
    # )
# dolaczenie do panelu admina modelu News
admin.site.register(Link,LinkAdmin)

class HeadLinkAdmin(admin.ModelAdmin):
    """
    header links
    """

    model = Link
    list_display = ('title','priority')
    search_fields = ('title',)

# dolaczenie do panelu admina modelu News
admin.site.register(HeadLink,HeadLinkAdmin)