from django.shortcuts import render
from .models import Link
# Create your views here.
def links(request):

    template = "links.html"
    # doctors_datest = []
    links = Link.objects.order_by('-priority')

    context = {
        'links':links,
    }
    # for doctors in doctors_datest:
    #     print(doctors['dates'].get_shirt_size_display())
    return render(request, template,context=context)
