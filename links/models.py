from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Link(models.Model):

    title = models.CharField(verbose_name=_("Temat"), max_length=1000)
    url = models.URLField(verbose_name=_("Link"))
    comment = models.TextField(verbose_name=_("Komentarz"),blank=True)
    priority = models.IntegerField(verbose_name=_("Priorytet"))
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _("Link")
        verbose_name_plural = _("Linki")

class HeadLink(models.Model):

    title = models.CharField(verbose_name=_("Etykieta"), max_length=24)
    url = models.URLField(verbose_name=_("Link"))
    show = models.BooleanField(verbose_name=_("Aktywny"), default=False)
    priority = models.IntegerField(verbose_name=_("Priorytet"))

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _("LinkEtykieta")
        verbose_name_plural = _("LinkiEtykiety")