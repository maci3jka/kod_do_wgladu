from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from datetimewidget.widgets import DateWidget
from django.db import models
from .models import UserProfile, Sick2user, Sickness


class UserProfileInline(admin.StackedInline):
    """
    dodanie do panelu admina dodatkowej funkcjonalnosci modelu UserProfile
    dodatkowe i edycja dane wymagane do rejestracji pacjenta
    sposob wyswietlania
    """
    model = UserProfile
    verbose_name_plural = 'users'
    dateTimeOptions = {
        'format': 'dd/mm/yyyy',
        'autoclose': True,
        'startView': 4,
        'language': 'pl',
        'startDate': '01/01/1990',

    }
    formfield_overrides = {
        models.DateField: {
            'widget': DateWidget(attrs={'id': "yourdatetimeid"},
                                 usel10n=True,
                                 bootstrap_version=3,
                                 options=dateTimeOptions
                                 )
        },
    }

    fieldsets = (
        (None, {'classes': ('wide',), 'fields': ('plec',
                                                 'data_urodzenia',
                                                 'phone_number',
                                                 'przychodnia'
                                                 )
                }
         ),
    )


class Sick2userInline(admin.StackedInline):
    """
    dodanie do panelu admina dodatkowej funkcjonalnosci modelu Sick2User
    dodawanie i edycja chorob do pacjenta
    sposob wyswietlania
    """
    model = Sick2user
    extra = 1
    verbose_name = 'Choroba'
    verbose_name_plural = 'Choroby'


class UserProfileAdmin(UserAdmin):
    """
    panel do dodania UserProfileInline, Sick2userInline do panelu admina
    """
    inlines = (UserProfileInline, Sick2userInline,)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username',
                       'password1',
                       'password2',
                       'first_name',
                       'last_name'
                       ),
        }),
    )

# polaczenie nowych modeli UserProfileInline, Sick2userInline do panelu admina w user
admin.site.unregister(User)
admin.site.register(User, UserProfileAdmin)


class Sick2userAdmin(admin.ModelAdmin):
    """
    dodanie do panelu admina dodatkowej funkcjonalnosci modelu Sick2User
    dodawanie i edycja chorob do pacjenta
    sposob wyswietlania
    """
    model = Sick2user
    ordering = ['user']
    list_display = ['user', 'sickness', 'sms_info', 'mail_info']

    fieldsets = (
        ('Połacz użytkowników i choroby', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('user',
                       'sickness',
                       ('sms_info', 'mail_info'),
                       ),
        }),
    )
# dolaczenie do panelu admina modelu sick2user
admin.site.register(Sick2user, Sick2userAdmin)


class SicknessAdmin(admin.ModelAdmin):
    """
    dodanie do panelu admina dodatkowej funkcjonalnosci modelu Sick2User
    dodawanie i edycja chorob do pacjenta
    sposob wyswietlania
    """

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name',
                       'about',
                       ),
        }),
    )
# dolaczenie do panelu admina modelu sickness
admin.site.register(Sickness, SicknessAdmin)
