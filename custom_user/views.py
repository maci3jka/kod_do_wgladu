from django.shortcuts import render, get_object_or_404, redirect
from .models import UserProfile,Sick2user

from .forms import UserProfileEditForm, UserEditForm, Sick2userEditForm, BaseSick2User
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
from django.forms.formsets import formset_factory


def contact(request):
    """
    Strona z kontaktami
    :param request: zadanie
    :return: strona
    """
    template = 'contact.html'
    return render(request, template)


def sickEdit(request):
    """
    Edytoanie przez uzytkownika swoich chorob
    :param request: zadanie
    :return: strona
    """
    template = "edit_sick.html"
    user = request.user
    SicknessFormSet = formset_factory(Sick2userEditForm, formset=BaseSick2User)
    user_sickness = Sick2user.objects.filter(user=user)

    sickness_data = [{'sickness': s.sickness, 'sms_info': s.sms_info, 'mail_info': s.mail_info}
                     for s in user_sickness]

    if request.method == 'POST':
        sickness_formset = SicknessFormSet(request.POST)

        new_sickness = []
        if sickness_formset.is_valid():
            for sickness_form in sickness_formset:
                    sickness = sickness_form.cleaned_data.get('sickness')
                    sms_info = sickness_form.cleaned_data.get('sms_info')
                    mail_info = sickness_form.cleaned_data.get('mail_info')

                    if sickness:
                        new_sickness.append(Sick2user(user=user,
                                                      sickness=sickness,
                                                      sms_info=sms_info,
                                                      mail_info=mail_info)
                                            )

            try:
                with transaction.atomic():
                    # Replace the old with the new
                    Sick2user.objects.filter(user=user).delete()
                    Sick2user.objects.bulk_create(new_sickness)

                    messages.success(request, 'Zmieniłeś swój profil.')

            except IntegrityError:
                messages.error(request, 'Wystąpił błąd przy zapisywaniu.')
                return redirect(reverse('sick'))

    else:
        sickness_formset = SicknessFormSet(initial=sickness_data)

    context = {'sickness_formset': sickness_formset, }
    return render(request, template, context=context)


def userEdit(request):
    """
    Edycja danyh uzytkownika
    :param request: zadanie
    :return: strona
    """

    template = "user_edit.html"
    cuser = get_object_or_404(UserProfile, user=request.user)
    uuser = request.user
    usrd = cuser.data_urodzenia

    if request.method == "POST":
        form = UserProfileEditForm(request.POST, instance=cuser)
        uform = UserEditForm(request.POST, instance=uuser)
        if form.is_valid() and uform.is_valid():
            post = form.save(commit=False)
            upost = uform.save(commit=False)
            post.user = request.user
            upost.user = request.user
            post.save()
            upost.save()
            return redirect('pref',)
    else:
        form = UserProfileEditForm(instance=cuser)
        uform = UserEditForm(instance=uuser)
    context = {'cru': usrd,
               'form': form,
               'uform': uform,
               }

    return render(request, template, context=context)
