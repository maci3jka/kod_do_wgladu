from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import User
from django.utils import timezone


class UserProfile(models.Model):
    """
    Poszerzenie podstawowego modelu User o dane potrzebne w przychodni
    """
    user = models.OneToOneField(User, null=False, default=1)
    PLEC_CHOICES = (('M', 'Mezczyżna'), ('K', 'Kobieta'))
    plec = models.CharField(max_length=1, choices=PLEC_CHOICES, unique=False, null=False)
    przychodnia_CHOICES = (('W', 'wielogłowy'), ('I', 'Iwkowa'))
    przychodnia = models.CharField(max_length=1, choices=przychodnia_CHOICES, unique=False, null=False, default='W')
    data_urodzenia = models.DateField(auto_now=False, auto_now_add=False, null=False, default=timezone.now,)
    phone_number = PhoneNumberField(blank=True,)

    def __str__(self):
        return "%s's profile" % self.user

    class Meta:
        verbose_name = "Użytkownik"
        verbose_name_plural = "Użytkownicy"

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


class Sickness(models.Model):
    """
    choroby lub schorzenia na ktore moze chorowac pacjent
    """
    name = models.CharField(max_length=50, unique=True, null=False)
    about = models.TextField(null=True)

    def __str__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = "Choroba"
        verbose_name_plural = "Choroby"


class Sick2user(models.Model):
    """
    Uzykonik moze chorowac na wiecej niz jedna chorobie (tworzy tablice asocjacyna)
    """
    user = models.ForeignKey(User)
    sickness = models.ForeignKey(Sickness)
    sms_info = models.BooleanField(null=False, default=False)
    mail_info = models.BooleanField(null=False, default=False)

    def __str__(self):
        return "%s - %s" % (self.user, self.sickness)

    class Meta:
        # together unique user & sickness
        unique_together = ("user", "sickness")
        verbose_name = "Choroba i użytkownik"
        verbose_name_plural = "Choroby i użytkownicy"
