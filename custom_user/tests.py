from django.test import TestCase
from .views import *
from django.contrib.auth.models import User
import datetime


class UserProfileModelTest(TestCase):

    def setUp(self):

        self.usr = User.objects.create(username='user1')

    def test_saving_and_retrieving_UserProfile(self):
        """
        dodanie rozszerzonych parametrow od uzytkownika
        :return:
        """
        #paramety
        pl='M'
        pr='W'
        pn="501763982"
        du=datetime.date(2010, 1, 1)
        #tworzenie modelu
        osoba1 = UserProfile(user=self.usr, plec=pl, przychodnia=pr, data_urodzenia= du,phone_number=pn)
        #zapisanie
        osoba1.save()
        #wyciągniecie z bazy
        seved_UserProfile = UserProfile.objects.all()
        #czy zostalo cos zapisane
        self.assertEqual(seved_UserProfile.count(),1)
        first_saved_item =seved_UserProfile[0]
        #sprawdzenie czy to te same modele
        self.assertEqual(first_saved_item, osoba1)

