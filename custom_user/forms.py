from __future__ import unicode_literals
from parsley.decorators import parsleyfy
from django import forms
from .models import UserProfile, Sick2user, Sickness
from django.forms.formsets import BaseFormSet
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from phonenumber_field.formfields import PhoneNumberField
from datetimewidget.widgets import DateWidget


class UserProfileEditForm(forms.ModelForm):
    """
    formularz od edycji danych dodatkowych uzytownika (Model UserProfile)
    """
    PLEC_CHOICES = (('M', 'Mezczyżna'), ('K', 'Kobieta'))
    plec = forms.ChoiceField(label=_('Płeć'), choices=PLEC_CHOICES)
    przychodnia_CHOICES = (('W', 'wielogłowy'), ('I', 'Iwkowa'))
    przychodnia = forms.ChoiceField(label=_('Przychodnia'), choices=przychodnia_CHOICES)
    phone_number = PhoneNumberField(label=_('Nr telefonu'), required=False)
    data_urodzenia = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3))

    class Meta:
        model = UserProfile
        fields = ('plec', 'data_urodzenia', 'phone_number', 'przychodnia')


class UserEditForm(forms.ModelForm):
    """
    formularz od edycji danych wymaganych uzytownika (Model User)
    """
    first_name = forms.CharField(label=_('Imię'), required=True)
    last_name = forms.CharField(label=_('Nazwisko'), required=True)
    email = forms.EmailField(required=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


@parsleyfy
class Sick2userEditForm(forms.ModelForm):
    """
    formularz do edycji i dodawania choroby uzytkownika (Model Sick2user)
    """
    sickness = forms.ModelChoiceField(label=_('Choroba'), queryset=Sickness.objects.all())
    sms_info = forms.CheckboxInput()
    mail_info = forms.CheckboxInput()

    class Meta:
        model = Sick2user
        fields = ('sickness', 'sms_info', 'mail_info')


class BaseSick2User(BaseFormSet):
    """
    formularz do obsugi wielu chorob
    """
    def clean(self):
        if any(self.errors):
            return

        sicknesses = []
        sms_infos = []
        mail_infos = []
        duplicates = False
        for form in self.forms:
            if form.cleaned_data:
                sickness = form.cleaned_data['sickness']
                sms_info = form.cleaned_data['sms_info']
                mail_info = form.cleaned_data['mail_info']

                if sickness:
                    if sickness in sicknesses:
                        duplicates = True
                    sicknesses.append(sickness)
                    sms_infos.append(sms_info)
                    mail_infos.append(mail_info)

                if duplicates:
                    raise forms.ValidationError('Choroby nie mogą się powtarzać', code='duplicate_links')

                if not sickness:
                    raise forms.ValidationError('musi byc wybrana jakas choroba.', code='missing_sickness')
