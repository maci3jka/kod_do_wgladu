
from django.shortcuts import render, get_object_or_404, redirect, Http404
from django.template import RequestContext

from .models import News
from .forms import NewsEditForm
from carusel.models import Carusel

def checkStaff(request):
    """
    spradza czy uzytkownik jest pracownikiem
    gdy nie jest odpowiedz to strona 404
    :param request:
    :return:
    """
    user = request.user
    if not user.is_staff:
        raise Http404


def home(request):
    """
    strona domowa
    newsy pobierane z bazy dlugosc tekstu okrojona do 50 slow
    :param request:
    :return:
    """
    template = 'home.html'

    news = News.objects.order_by('-timestamp')
    carousels = Carusel.objects.order_by('-timestamp')

    carusel_data = ['Zapraszamy od przychodni w Wielogłowach!', 'Zapraszamy do przychodni w Iwkowej!']

    context = {
        'carusel_data': carusel_data,
        'carousels': carousels,
        'news': news
    }

    return render(request, template, context=context, context_instance=RequestContext(request))


def newsPage(request, news_id):
    """
    strona z newsem
    :param request:
    :param news_id:id newsa
    :return:
    """
    template = 'news.html'
    news = get_object_or_404(News, id=news_id)

    context = {'news': news,
               }

    return render(request, template, context=context)


def newsEdit(request, news_id=None):
    """
    edycja lub dodanie newsa
    :param request:
    :param news_id:id edytowanego newsa
    :return:
    """
    checkStaff(request)

    template = 'news_add.html'
    if news_id:
        news = get_object_or_404(News, id=news_id)
    else:
        news = None

    form = NewsEditForm(request.POST or None, request.FILES or None, instance=news)
    if form.is_valid():
        form.save()
        return redirect('home')

    context = {'form': form, }

    return render(request, template, context=context)


def newsList(request):
    """
    panel do edycji newsow
    :param request:
    :return:
    """

    checkStaff(request)

    template = "news_list.html"

    to_delete = request.POST.getlist('delete_box')
    print(to_delete)
    if to_delete:
        News.objects.all().filter(id__in=to_delete).delete()

    news_objects = News.objects.values('id', 'title', 'timestamp').order_by('-timestamp')

    context = {'news_objects': news_objects,}
    return render(request, template,context=context)
