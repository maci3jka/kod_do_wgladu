from django.contrib import admin
from django.db import models
from .models import News
from .forms import ImageThumbnailFileInput

class NewsAdmin(admin.ModelAdmin):
    """
    dodanie do panelu admina dodatkowej funkcjonalnosci modelu Sick2User
    dodawanie i edycja newsow
    sposob wyswietlania
    """
    formfield_overrides = {
        models.ImageField: {
            'widget': ImageThumbnailFileInput()
        },
    }
    add_fieldsets = (
        (None, {
            'fields': ('plec',
                       ('przychodnia',
                       'sickness'),
                       'data_urodzenia_max',
                       'data_urodzenia_min',
                       'text',
                       'image',
                       ('title','subtitle'),
                       ),
        }),
    )
# dolaczenie do panelu admina modelu News
admin.site.register(News,NewsAdmin)
