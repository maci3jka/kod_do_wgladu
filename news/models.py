
from django.db import models
from custom_user.models import Sickness
from django.utils.translation import ugettext_lazy as _


class News(models.Model):
    """
    Newsy z dodatkowymi parametrami dla oraniczenin wyswietlania i zawiadamiania szegolnych grup uzytkownikow
    """
    # dane newsa
    text = models.TextField(verbose_name=_("Tekst"), null=False)
    image = models.ImageField(verbose_name=_("Obrazek"), upload_to='news', blank=True)
    title = models.CharField(verbose_name=_("Tytuł"), max_length=50, unique=True, null=False,)
    subtitle = models.CharField(verbose_name=_("Podtytuł"), max_length=50, unique=False, blank=True,)
    # dodatkowe ograniczenia
    PLEC_CHOICES = (('M', 'Mezczyżna'), ('K', 'Kobieta'))
    plec = models.CharField(verbose_name=_("Płeć"), max_length=1, choices=PLEC_CHOICES, unique=False, blank=True)
    przychodnia_CHOICES = (('W', 'wielogłowy'), ('I', 'Iwkowa'))
    przychodnia = models.CharField(verbose_name=_("Przychodnia"), max_length=1, choices=przychodnia_CHOICES,
                                   unique=False, blank=True)
    data_urodzenia_max = models.DateField(verbose_name=_("Data najstarszego urodzenia"), auto_now=False,
                                          auto_now_add=False, blank=True, null=True)
    data_urodzenia_min = models.DateField(verbose_name=_("Data najmłodszego urodzenia"), auto_now=False,
                                          auto_now_add=False, blank=True, null=True)
    sickness = models.ForeignKey(Sickness, verbose_name=_("Choroba"), blank=True, null=True)

    # czas stworzenia i aktualizacji
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _("News")
        verbose_name_plural = _("Newsy")
