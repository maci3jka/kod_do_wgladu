from django.test import TestCase
from news.models import News
from django.db.utils import IntegrityError
import datetime
# Create your tests here.


class NewsModelTest(TestCase):
    """
    klasa testujaca model
    """

    def get_news(self, pl='M', pr='W', dmax=datetime.date(2010, 1, 1), dmin=datetime.date(2000, 1, 1),
                 txt="dsjflsaijfdasoijhfd", title="sdj", subtitle="sdfsdfsdggh!", im=None):
        """
        pomocnicza funkcja tworzaca poprawny news
        szybkosci brak obrazka
        """
        return News(plec=pl, przychodnia=pr, data_urodzenia_max=dmax, data_urodzenia_min=dmin, text=txt, image=im,
                     title=title, subtitle=subtitle )

    def test_creation(self):
        """
        test tworzenia
        sprawdza czy news jest instancja newsa
        """
        n = self.get_news()
        self.assertTrue(isinstance(n, News))
        self.assertEqual(n.__str__(), n.title)

    def test_saving_and_retrieving_News(self):
        """
        zapisywanie i odczytywanie newsa
        :return:
        """
        #tworzenie modelu
        news1 = self.get_news()
        seved_News = News.objects.all()
        self.assertEqual(seved_News.count(),0)

        #zapisanie
        news1.save()
        #wyciągniecie z bazy
        seved_News = News.objects.all()
        #czy zostalo cos zapisane
        self.assertEqual(seved_News.count(),1)
        first_saved_news =seved_News[0]
        #sprawdzenie czy to te same modele
        self.assertEqual(first_saved_news, news1)
        self.assertEqual(first_saved_news.__str__(), news1.__str__())

    #sprawdzenie wartosci not null
    def test_integrity_error(self):
        """
        test sprawdzajacy blad dla tekstu ktory nie moze byc null
        """
        with self.assertRaises(IntegrityError):
            m = self.get_news(txt=None)
            m.save()

    def test_integrity_error(self):
        """
        test sprawdzajacy blad dla tekstu ktory nie moze byc null
        """
        with self.assertRaises(IntegrityError):
            n = self.get_news(title=None)
            n.save()