from django.test import TestCase
from news.models import News
from news.forms import NewsEditForm
import datetime


class NewsEditFormTest(TestCase):
    """
    testowanie formularza
    """

    def give_news(self, pl='M', pr='W', dmax=datetime.date(2010, 1, 1), dmin=datetime.date(2000, 1, 1),
                  txt="dsjflsaijfdasoijhfd", title="sdj", subtitle="sdfsdfsdggh!", im=None):
        """
        pomocnicza funkcja tworzaca przykladowego newsa
        """
        return News(plec=pl, przychodnia=pr, data_urodzenia_max=dmax, data_urodzenia_min=dmin, text=txt, image=im,
                     title=title, subtitle=subtitle)

    def test_valid_form(self):
        """
        sprawdzanie przyjecia poprawnego formularza
         zdjecie pominieto ze wzgledu na rozmiar i szybkosc
        """
        n = self.give_news(title="forms_test")
        data = {'title': n.title, 'text': n.text}
        form = NewsEditForm(data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        """
        sprawdzenie odrzucenia niepoprawnego formularza
        """
        n = self.give_news(title="forms_test")
        data = {'title': "", 'text': n.text}
        form = NewsEditForm(data)
        self.assertFalse(form.is_valid())

    def test_save_form(self):
        """
        zapisanie  poprawnego formularza
        """
        n = News(title="forms_test", text="sdfdsfr")
        data = {'title': n.title, 'text': n.text}
        form = NewsEditForm(data)
        form.save()
        news = News.objects.last()
        # seria sprawdzen poprawnosci
        self.assertEqual(n.plec, news.plec)
        self.assertEqual(n.przychodnia, news.przychodnia)
        self.assertEqual(n.data_urodzenia_max, news.data_urodzenia_max)
        self.assertEqual(n.data_urodzenia_min, news.data_urodzenia_min)
        self.assertEqual(n.text, news.text)
        self.assertEqual(n.image, news.image)
        self.assertEqual(n.title, news.title)
        self.assertEqual(n.subtitle, news.subtitle)
        self.assertEqual(n.sickness, news.sickness)
        self.assertNotEqual(n.timestamp, news.timestamp)
        self.assertNotEqual(n.updated, news.updated)
