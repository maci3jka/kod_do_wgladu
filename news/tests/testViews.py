from django.test import TestCase
from news.views import *
from news.models import News
from django.core.urlresolvers import resolve
from django.core.urlresolvers import reverse
# Create your tests here.


class NewsViewsTest(TestCase):
    """
    klasa testujaca view
    """
    # sprawdzenie poprawnosci funkcji i url

    #       url(r'^$', newsViews.home, name='home'),
    def test_root_url_resolves_to_home_view(self):
        """
        sprawdza czy poprawna funkcja home
        """
        found = resolve('/')
        self.assertEqual(found.func, home)

    # url(r'^news/(?P<news_id>[0-9]+)/$', newsViews.newsPage, name='news'),
    def test_resolves_newsPage(self):
        """
        sprawdza czy poprawna funkcja newsPage
        """
        # towrzenie newsa
        News.objects.create(title="sdfsd", text="dsfsfs")
        n_id = News.objects.last().id
        found = resolve('/news/%i/' % n_id)
        self.assertEqual(found.func, newsPage)

    # url(r'^news_add/', newsViews.newsEdit, name='news_add'),
    def test_resolves_newsAdd(self):
        """
        sprawdza czy poprawna funkcja newsPage
        """
        found = resolve('/news_add/')
        self.assertEqual(found.func, newsEdit)

    # url(r'^news_edit/(?P<news_id>[0-9]+)/$', newsViews.newsEdit, name='news_edit'),
    def test_resolves_newsEdit(self):
        """
        sprawdza czy poprawna funkcja newsPage
        """
        # towrzenie newsa
        News.objects.create(title="sdfsd", text="dsfsfs")
        n_id = News.objects.last().id

        found = resolve('/news_edit/%i/' % n_id)
        self.assertEqual(found.func, newsEdit)

    # url(r'^news_list/', newsViews.newsList, name='news_list'),
    def test_resolves_newsList(self):
        """
        sprawdza czy poprawna funkcja newsPage
        """
        found = resolve('/news_list/')
        self.assertEqual(found.func, newsList)

    # koniec sprawdzania uzycia wlasciwej funkcji

    # sprawdzenie wyświetlania dla braku newsow

    def test_home_page_without_news(self):

        to_find = "Brak news-ów do wyświetlenia"
        response = self.client.get(reverse('home'))
        self.assertContains(response, to_find)

    # sprawdzanie czy strony zawieraja dane newsy

    # strona domowa
    def test_home_contains_news(self):
        tit1 = 'dsfddsfsfd'
        txt1 = 'sdfsdffdsfds'
        tit2 = 'sdfsdfsd'
        # teost powinien byc uciety do 50 slow
        txt2 = "dslk dslk dslk dslk dslk dslk " \
               "dslk dslk dslk dslk dslk dslk " \
               "dslk dslk dslk dslk dslk dslk " \
               "dslk dslk dslk dslk dslk dslk " \
               "dslk dslk dslk dslk dslk dslk " \
               "dslk dslk dslk dslk dslk dslk " \
               "dslk dslk dslk dslk dslk dslk "

        News.objects.create(title=tit1, text=txt1)
        News.objects.create(title=tit2, text=txt2)

        response = self.client.get(reverse('home'))
        self.assertContains(response, tit1)
        self.assertContains(response, text=tit1[0:49])
        self.assertContains(response, tit2)
        self.assertContains(response, text=txt2[0:49])

    # strona newsa
    def test_news_contains_news(self):
        tit1 = 'dsfddsfsfd'
        txt1 = 'sdfsdffdsfds'
        News.objects.create(title=tit1, text=txt1)

        n_id = News.objects.last().id
        response = self.client.get('/news/%i/' % n_id)
        self.assertContains(response, tit1)
        self.assertContains(response, text=tit1)
