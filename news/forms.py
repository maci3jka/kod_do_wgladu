from __future__ import unicode_literals
from django import forms
from .models import News
from django.utils.translation import ugettext_lazy as _
from custom_user.models import Sickness
from datetimewidget.widgets import DateWidget
from floppyforms import ClearableFileInput


class ImageThumbnailFileInput(ClearableFileInput):
    """
    miniaturka obrazka dla panelu admina
    """
    template_name = 'floppyforms/image_thumbnail.html'


class NewsEditForm(forms.ModelForm):
    """
    formularz dla dodawania newsa
    """
    PLEC_CHOICES = (('M', 'Mezczyżna'), ('K', 'Kobieta'))
    plec = forms.ChoiceField(label=_("Płeć"), choices=PLEC_CHOICES, required=False)
    przychodnia_CHOICES = (('W', 'wielogłowy'), ('I', 'Iwkowa'))
    przychodnia = forms.ChoiceField(label=_("Przychodnia"), choices=przychodnia_CHOICES, required=False)
    data_urodzenia_max = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3), required=False)
    data_urodzenia_min = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3), required=False)
    text = forms.CharField(label=_("Tekst"), widget=forms.Textarea(attrs={'placeholder': 'Wpisz tekst'}), required=True)
    image = forms.ImageField(label=_("Obrazek"), widget=ImageThumbnailFileInput, required=False)
    title = forms.CharField(label=_('Tytuł'), required=True)
    subtitle = forms.CharField(label=_('Podtytuł'), required=False)
    sickness = forms.ModelChoiceField(label=_('Choroba'), queryset=Sickness.objects.all(), required=False)

    class Meta:
        model = News
        fields = ('plec', 'przychodnia', 'sickness', 'data_urodzenia_max', 'data_urodzenia_min', 'text',
                  'title', 'subtitle', 'image')
