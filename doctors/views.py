# -*- coding: utf-8 -*-
# Create your views here.
from django.shortcuts import render, get_object_or_404, redirect, Http404
from django.template import RequestContext

from .models import Doctor, DoctorDates

def doctors_dates(request):

    template = "doctors_dates.html"
    # doctors_datest = []
    doctors = Doctor.objects.order_by('surname')

    doctors_datest_I = []
    doctors_datest_W = []
    for doctor in doctors:
        dates_I = DoctorDates.objects.filter(doctor=doctor,  przychodnia="I")
        dates_W = DoctorDates.objects.filter(doctor=doctor,  przychodnia="W")
        if len(dates_I) > 0:
            doctors_datest_I.append({'doctor': doctor, 'dates': dates_I})
        if len(dates_W) > 0:
            doctors_datest_W.append({'doctor': doctor, 'dates': dates_W})
    context = {
        'doctors_datest_I':doctors_datest_I,
        'doctors_datest_W':doctors_datest_W,
    }
    # for doctors in doctors_datest:
    #     print(doctors['dates'].get_shirt_size_display())
    return render(request, template,context=context)
