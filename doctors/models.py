# -*- coding: utf-8 -*-

# Create your models here.
# docktor
# data
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Doctor(models.Model):

    title = models.CharField(verbose_name=_("Tytuł"), max_length=1000)
    name = models.CharField(verbose_name=_("Imię"), max_length=1000)
    surname = models.CharField(verbose_name=_("Nazwisko"), max_length=1000)
    extras = models.TextField(verbose_name=_("Dodatkowe uwagi"), blank=True)
    urlop = models.BooleanField(verbose_name=_("Urlop"),blank=False, default=False)
    urlop_start = models.DateTimeField(verbose_name=_("Urlop początek"), default=None)
    urlop_end = models.DateTimeField(verbose_name=_("Urlop koniec"), default=None)

    # czas stworzenia i aktualizacji
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str("%s %s %s" % (self.title, self.name, self.surname))

    class Meta:
        verbose_name = _("Lekarz")
        verbose_name_plural = _("Lekarze")


class DoctorDates(models.Model):
    """
    dates for doctors
    """
    weekdays_CHOICES = (('PN', 'Poniedziałek'), ('WT', 'Wtorek'), ('SR', _('Środa')), ('CZ', 'Czwartek'), ('PT', 'Piątek'), ('SO', 'Sobota'), ('ND', 'Niedziela'))
    weekdays = models.CharField(verbose_name=_("Dzień tygodnia"), max_length=2, choices=weekdays_CHOICES,
                                   unique=False, blank=False)

    start_time = models.TimeField(verbose_name=_("Rozpoczęcie"),auto_now_add=False, auto_now=False, unique=False, blank=False)
    stop_time = models.TimeField(verbose_name=_("Zakończenie"),auto_now_add=False, auto_now=False, unique=False, blank=False)

    przychodnia_CHOICES = (('W', 'Wielogłowy'), ('I', 'Porąbka Iwkowska'))
    przychodnia = models.CharField(verbose_name=_("Przychodnia"), max_length=1, choices=przychodnia_CHOICES,
                                   unique=False, blank=False)

    wizyta_CHOICES = (('D', 'wizyta domowa'), ('P', 'wizyta w gabinecie'))
    wizyta = models.CharField(verbose_name=_("Rodzaj Wizyty"), max_length=1, choices=wizyta_CHOICES,
                                   unique=False, blank=False)

    extras = models.TextField(verbose_name=_("Dodatkowe uwagi"), blank=True)

    doctor = models.ForeignKey(Doctor)

    # czas stworzenia i aktualizacji
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    # def __str__(self):
    #     return str("%s %" % ())

    class Meta:
        verbose_name = _("Wizyta")
        verbose_name_plural = _("Wizyty")

