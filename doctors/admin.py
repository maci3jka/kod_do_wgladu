# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Doctor, DoctorDates
# Register your models here.
class DoctorsAdmin(admin.ModelAdmin):
    """
    dodawanie lekarzy
    """
    # formfield_overrides = {
    #     models.ImageField: {
    #         'widget': ImageThumbnailFileInput()
    #     },
    # }
    model = Doctor
    list_display = ('name','surname')
    search_fields = ('name','surname')
    list_filter = ('name','surname')
    add_fieldsets = (
        (None, {
            'fields': ('title',
                       'name',
                       'surname',
                       'extras',
                       ),
        }),
    )
# dolaczenie do panelu admina modelu News
admin.site.register(Doctor,DoctorsAdmin)

class DoctorDatesAdmin(admin.ModelAdmin):
    model = DoctorDates
    list_display = ('doctor',
                    'weekdays',
                    'start_time',
                    'stop_time',
                    'przychodnia',
                        'wizyta', )
    add_fieldsets = (
        (None, {
            'fields': ('weekdays',
                       'start_time',
                       'stop_time',
                       'przychodnia',
                       'wizyta',
                       'doctor',
                       'extras',
                       ),
        }),
    )

admin.site.register(DoctorDates,DoctorDatesAdmin)